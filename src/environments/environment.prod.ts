export const environment = {
  production: true,
  apiUrl: 'http://capsule-api.lss1.backbone.education:5000/v1',
  keycloak: {
    // Url of the Identity Provider
    url: 'http://capsule-api.lss1.backbone.education/auth/',

    // Realm
    realm: 'ac-ver-test',

    // The SPA's id. 
    // The SPA is registerd with this id at the auth-serverß
    clientId: 'front-api',

  },
  status: 0
};
