import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js/dist/Chart.js';

@Component({
  selector: 'app-addons-chart',
  templateUrl: './addons-chart.component.html',
  styleUrls: ['./addons-chart.component.scss']
})
export class AddonsChartComponent implements OnInit {

  chart = []
  @ViewChild('canvas') canvasRef: ElementRef;

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {

    let canvas = this.elementRef.nativeElement.querySelector(`canvas`)
    // TODO find something useful to put on the chart
    this.chart = new Chart(canvas, {
      type: 'doughnut',
      data: {
        datasets: [{
          data: [10, 20, 30]
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: [
          'Red',
          'Yellow',
          'Blue'
        ]
      }
    });
  }

}
