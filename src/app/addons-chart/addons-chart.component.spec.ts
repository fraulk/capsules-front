import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonsChartComponent } from './addons-chart.component';

describe('AddonsChartComponent', () => {
  let component: AddonsChartComponent;
  let fixture: ComponentFixture<AddonsChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonsChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonsChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
