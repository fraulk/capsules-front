import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';
import { CapsuleOverviewComponent } from './capsule-overview/capsule-overview.component';
import { WebappComponent } from './webapp/webapp.component';
import { AddCapsuleComponent } from './add-capsule/add-capsule.component';
import { RuntimesComponent } from './runtimes/runtimes.component';
import { AddAddonsComponent } from './add-addons/add-addons.component';
import { AddonInfoComponent } from './addon-info/addon-info.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ApptokensComponent } from './apptokens/apptokens.component';
import { SshKeysComponent } from './ssh-keys/ssh-keys.component';
import { AppAuthGuard } from './app.authguard';


const routes: Routes = [
  { path: '', redirectTo: '/overview', pathMatch: 'full' },
  { path: 'overview', component: CapsuleOverviewComponent },
  { path: 'webapp', component: WebappComponent },
  { path: 'add-capsule', component: AddCapsuleComponent }, //, canActivate: [AppAuthGuard], data: { roles: ['admin'] }
  { path: 'runtimes', component: RuntimesComponent },
  { path: 'add-addons', component: AddAddonsComponent },
  { path: 'addon', component: AddonInfoComponent },
  { path: 'apptokens', component: ApptokensComponent },
  { path: 'ssh-keys', component: SshKeysComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AppAuthGuard]
})
export class AppRoutingModule { }
