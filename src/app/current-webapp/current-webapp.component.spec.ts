import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentWebappComponent } from './current-webapp.component';

describe('CurrentWebappComponent', () => {
  let component: CurrentWebappComponent;
  let fixture: ComponentFixture<CurrentWebappComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentWebappComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentWebappComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
