import { Component, OnInit, Input, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { CapsuleService } from '../capsule.service';
import { Webapp } from '../objects/webapp';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Router } from '@angular/router';

@Component({
  selector: 'app-current-webapp',
  templateUrl: './current-webapp.component.html',
  styleUrls: ['./current-webapp.component.scss']
})
export class CurrentWebappComponent implements OnInit {
  @Input() capsuleId: string
  @Input() imgPath: string
  @Input() hasWebapp: boolean = true
  @Input('cdkDragFreeDragPosition') freeDragPosition: { x: number; y: number; }
  webapp: Webapp
  runtime: any
  webappName: string = ""
  defaultPosition = { x: 0, y: 0 }
  resetDisabled: boolean = true
  isOverview: boolean
  isApiCallDone: boolean = false

  constructor(private capsuleService: CapsuleService, private cd: ChangeDetectorRef, public router: Router) { }

  getWebapp(capsuleId): void {
    this.capsuleService.getWebapp(capsuleId, "current webapp").subscribe(
      webapp => this.webapp = webapp,
      (err) => console.log(err),
      () => {
        this.isApiCallDone = true
        if (this.webapp == null) {
          this.hasWebapp = false
          return
        } else this.hasWebapp = true
        this.getRuntimesFam(this.webapp.runtime_id)
      }
    );
  }

  getRuntimesFam(runId) {
    this.capsuleService.getRuntime(runId, "current webapp").subscribe(
      runtime => this.runtime = runtime,
      (err) => console.log(err),
      () => {
        this.webappName = this.runtime.fam
        this.setWebappImg(this.webappName)
        this.cd.detectChanges()
      }
    )
  }

  resetPosition() {
    this.freeDragPosition = { x: 0, y: 0 };
    this.resetDisabled = true
    localStorage.setItem('CurrWebappCardPos', JSON.stringify(this.freeDragPosition))
  }

  afterDrag($ev: CdkDragEnd) {
    this.freeDragPosition = $ev.source.getFreeDragPosition()
    if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
    localStorage.setItem('CurrWebappCardPos', JSON.stringify(this.freeDragPosition))
  }

  ngOnInit(): void {
    this.getWebapp(this.capsuleId)
    if (this.router.url == "/overview") this.isOverview = true //Check the route to know if it's /overview, and add drag stuff if it is
    if (this.isOverview) {
      this.freeDragPosition = JSON.parse(localStorage.getItem('CurrWebappCardPos'))
      if (this.freeDragPosition == undefined) this.freeDragPosition = { x: 0, y: 0 }
      if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
    } else
      this.freeDragPosition = { x: 0, y: 0 }
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      let chng = changes[propName]
      if (!chng.firstChange) {
        if (this.hasWebapp == true) this.getWebapp(this.capsuleId)
      }
    }
  }

  setWebappImg(wbpName: string) {
    switch (wbpName.toLowerCase()) {
      case "apache php":
        this.imgPath = "../assets/square-php-apache.svg";
        break;
      case "nodejs":
        this.imgPath = "../assets/square-nodejs.svg";
        break;
      case "ruby":
        this.imgPath = "../assets/square-ruby.svg";
        break;
      default:
        this.imgPath = "";
        break;
    }
  }

}
