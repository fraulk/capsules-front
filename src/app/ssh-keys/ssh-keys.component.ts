import { Component, OnInit, TemplateRef } from '@angular/core';
import { CapsuleService } from '../capsule.service';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-ssh-keys',
  templateUrl: './ssh-keys.component.html',
  styleUrls: ['./ssh-keys.component.scss']
})
export class SshKeysComponent implements OnInit {
  sshKeys: any[]
  isFetched: boolean
  error: any
  errorMsg1: any
  key: any
  sshKeyToShow: string = ""


  getSSHKeys() {
    this.capsuleService.getSSHKeys("ssh-keys").subscribe(
      (keys) => this.sshKeys = keys,
      (err) => console.log(err),
      () => {
        if (this.sshKeys != null) this.isFetched = true
      }
    )
  }

  postSSHKey(sshkey) {
    this.capsuleService.addSSHKey(sshkey, "ssh-keys").subscribe(
      (token) => this.key = token,
      (err) => console.log(err),
      () => {
        if (this.key != null) {
          if (this.sshKeys == null) this.sshKeys = [this.key]
          else this.sshKeys.push(this.key)
        }
      }
    )
  }

  deleteSSHKey(id) {
    this.capsuleService.deleteSSHKey(id, "ssh-keys").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.getSSHKeys()
      }
    )
  }

  constructor(private capsuleService: CapsuleService, private toastrService: NbToastrService, public translate: TranslateService, private dialogService: NbDialogService, private titleService: Title) { }

  /**
   * Checks the key validity and post it
   * @param key ssh key to add
   */
  onSend(key) {
    let inputControl = key.match(/ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} ([^@]+@[^@]+)/gm)
    if (key == null || key == '' || key == undefined || inputControl != key) {
      this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' });
      return
    }
    let sshkey = {
      "public_key": key
    }
    this.postSSHKey(sshkey)
  }

  delete(id) {
    this.deleteSSHKey(id)
  }

  open(dialog: TemplateRef<any>, sshkey: string) {
    this.sshKeyToShow = sshkey
    this.dialogService.open(dialog)
  }

  /**
   * Create an invisible textearea, put the ssh key in, copy, and remove it
   * @param val ssh key to copy
   */
  copy(val) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  close(dialog) {
    dialog.close()
  }

  ngOnInit(): void {
    this.getSSHKeys()
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validSSHKey').subscribe(res => this.errorMsg1 = res)
    this.titleService.setTitle("Capsule Front - SSH Keys")
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validSSHKey').subscribe(res => this.errorMsg1 = res)
    });
  }

}
