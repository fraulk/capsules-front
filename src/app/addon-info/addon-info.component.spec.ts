import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonInfoComponent } from './addon-info.component';

describe('AddonInfoComponent', () => {
  let component: AddonInfoComponent;
  let fixture: ComponentFixture<AddonInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
