import { Component, OnInit, Input, SimpleChanges, EventEmitter, Output, TemplateRef } from '@angular/core';
import { Addons } from '../objects/addons';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { CapsuleService } from '../capsule.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-addon-info',
  templateUrl: './addon-info.component.html',
  styleUrls: ['./addon-info.component.scss']
})
export class AddonInfoComponent implements OnInit {
  @Input() addons: Addons[]
  @Output() needRefresh = new EventEmitter()
  addonName: string = ""
  selectedAddon: any
  hasAddon: boolean = true
  editMode: boolean = false
  addonCardAccent: string = "primary"
  success: string; successMsg: string;
  error: string; errorMsg: string; errorMsg2: any;
  imgPath: string = ''
  runtime: any
  availableOpts: any[]
  options: any[] = []
  fileReader: FileReader = new FileReader()
  fileToShow: string = ""

  constructor(private route: ActivatedRoute, private toastrService: NbToastrService, public translate: TranslateService, private capsuleService: CapsuleService, private router: Router, private dialogService: NbDialogService, private titleService: Title) { }

  /**
   * Get runtime with specified id to get info on actual addon (name, description, family, available options) then set img pathand organize option if there is
   * @param runId runtime id
   */
  getRuntimesFam(runId: string) {
    this.capsuleService.getRuntime(runId, "addon-info").subscribe(
      runtime => {
        this.runtime = runtime
        this.availableOpts = (runtime.available_opts.length != 0) ? runtime.available_opts : null
      },
      (err) => console.log(err),
      () => {
        this.setImg(this.runtime.fam)
        if (this.availableOpts != null)
          this.availableOpts = this.organizeOpts()
      }
    )
  }

  deleteAddon(capsuleId: string, addonId: string) {
    this.capsuleService.deleteAddon(capsuleId, addonId, "addon-info").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        setTimeout(() => {
          this.needRefresh.emit()
          setTimeout(() => {
            this.router.navigate(['/overview'])
          }, 500);
        }, 500);
      }
    )
  }

  /**
   * Put an addon and then replaces selected addons info with addon in parameter
   * @param capsuleId capsule id
   * @param addonId addon id
   * @param addon addon object
   */
  putAddon(capsuleId: string, addonId: string, addon) {
    this.capsuleService.putAddon(capsuleId, addonId, addon, "addon-info").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.selectedAddon.name = addon.name
        this.selectedAddon.description = addon.description
        if (addon.opts != undefined && addon.opts.length != 0)
          this.selectedAddon.opts = addon.opts
      }
    )
  }

  ngOnInit(): void {
    // Gets the addon's name in the url params by subscribing to it
    this.route.queryParamMap.subscribe(
      params => {
        this.addonName = params.get("name")
        this.setAddon()
      }
    )
    this.setAddon()
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg = res)
    this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg2 = res);
    this.titleService.setTitle("Capsule Front - Addons")
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg = res)
      this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg2 = res);
    });
  }

  edit() {
    this.editMode = true
    this.addonCardAccent = "warning"
  }

  cancel() {
    this.editMode = false
    this.addonCardAccent = "primary"
  }

  delete() {
    this.deleteAddon(this.selectedAddon.capsule_id, this.selectedAddon.id)
  }

  /**
   * Check the input, create the addon object, add options if there is, and put the object to the api
   * @param name addon's new name
   * @param description addon's new description
   */
  save(name: string, description: string) {
    let inptControl = (name.match(/[a-z0-9\-_]+/m) != null) ? name.match(/[a-z0-9\-_]+/m)[0] : ""
    if (name == inptControl) {
      let addon = {
        "name": (name == "") ? this.selectedAddon.name : name,
        "description": (description == "") ? this.selectedAddon.description : description,
        "runtime_id": this.selectedAddon.runtime_id
      }
      if (this.options.length != 0)
        addon['opts'] = this.options
      console.log(addon)
      this.putAddon(this.selectedAddon.capsule_id, this.selectedAddon.id, addon)
      this.toastrService.show(this.successMsg, this.success, { status: 'success' });
    }
    else {
      this.toastrService.show(this.errorMsg, this.error, { status: 'danger' });
      return
    }
    this.editMode = false
    this.options = []
    this.addonCardAccent = "success"
  }

  /**
   * Used for converting file in base 64
   * @param file file to convert in base 64
   */
  toBase64 = file => new Promise((resolve, reject) => {
    this.fileReader.readAsDataURL(file);
    this.fileReader.onload = () => resolve(this.fileReader.result);
    this.fileReader.onerror = error => reject(error);
  });

  /**
   * Saves the data for each options.
   * If option array is empty, push a new element; else it search for an existing option and put the value on or delete it if value's empty; else create an element if not found
   * @param field_name the unique field name
   * @param tag the tag of the option
   * @param value the input value
   * @param file true if it's a file
   */
  // TODO input control for options
  async saveOptions(field_name: string, tag: string, value: any, file: boolean = false) {
    if (file) { value = await this.toBase64(value[0]) }
    if (value == true || value == false) { value = `${value}` }
    let found = -1
    if (this.options.length == 0)
      this.options.push({ "field_name": field_name, "tag": tag, "value": value })
    else {
      this.options.map((item, i) => {
        if (item.field_name == field_name) {
          found = i
          return
        }
      })
      if (found > -1) {
        if (value != "")
          this.options[found].value = value
        else
          this.options.splice(found, 1)
      }
      else
        this.options.push({ "field_name": field_name, "tag": tag, "value": value })
    }
  }

  /**
   * If list of addons gave by parent component exists, map it and try to find the addon named like the url parameters one, then get runtimes info
   */
  setAddon() {
    if (this.addons) {
      this.hasAddon = false
      this.addons.map((item, i) => {
        if (item.name == this.addonName) {
          this.selectedAddon = item
          this.options = this.selectedAddon.opts
          this.hasAddon = true
          return
        }
      })
      if (this.selectedAddon != undefined) this.getRuntimesFam(this.selectedAddon.runtime_id)
    }
  }

  /**
   * Organize array sorted by tags
   * @returns Returns a list of tags
   */
  organizeOpts(): any[] {
    let tags = []
    let tagsName
    let newTags: object = {}
    let keys = []

    this.availableOpts.map(item => tags.push(item.tag))
    tagsName = [...new Set(tags)] // removes duplicates
    tagsName.map(item => newTags[item] = []) // create an array of object with tag name as key
    tags = []
    tags.push(newTags)
    keys = Object.keys(tags[0]) // create an array of keys as string
    this.availableOpts.map(item => {
      keys.map(key => {
        if (item.tag == key)  // if the tag match the key, push all the stuff into our organized array
          tags[0][key].push(item)
        this.selectedAddon.opts.map(addonOpts => {
          if (item.tag == addonOpts.tag) { // if it matches the same tags between selected addons tags and available opts tags
            tags[0][key].map((itemOpts, i) => { // map the previously created array
              if (itemOpts.field_name == addonOpts.field_name) // if available options field names matches selected addons field name
                itemOpts['current_value'] = addonOpts.value // add the current value field to remember previous data
            })
          }
        })
      })
    })
    tags.push(tagsName)
    console.log(tags)
    return tags
  }

  /**
   * Shows an existent file in the addon's options
   * @param dialog nb-dialog html element
   * @param file file data from options
   */
  open(dialog: TemplateRef<any>, file: string) {
    file = file.substr(file.indexOf(',') + 1)
    this.fileToShow = atob(file)
    this.dialogService.open(dialog)
  }

  close(dialog) {
    dialog.close()
  }

  openDeleteConfirmation(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog)
  }

  closeDeleteConfirm(dialog) {
    this.delete()
    dialog.close()
  }

  /**
   * Choose the right path to show images
   * @param name runtime family name
   */
  setImg(name: string) {
    switch (name.toLowerCase()) {
      case "mysql":
        this.imgPath = "../assets/square-mysql.svg"
        break;
      case "postgresql":
        this.imgPath = "../assets/square-psql.svg"
        break;
      case "redis":
        this.imgPath = "../assets/square-redis.svg"
        break;
      case "memcached":
        this.imgPath = "../assets/square-mem.svg"
        break;
      default:
        this.imgPath = "";
        break;
    }
  }

}
