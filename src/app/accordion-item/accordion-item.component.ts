import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { Addons } from '../objects/addons'
import { CapsuleService } from '../capsule.service';
import { CdkDragEnd } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-accordion-item',
  templateUrl: './accordion-item.component.html',
  styleUrls: ['./accordion-item.component.scss']
})
export class AccordionItemComponent implements OnInit {
  @Input() capsuleId: string
  @Input() hasAddons: boolean = true
  @Input('cdkDragFreeDragPosition') freeDragPosition: { x: number; y: number; }
  addons: any[] = null
  imgPath: string
  runtimeFam: any
  defaultPosition = { x: 0, y: 0 }
  resetDisabled: boolean = true

  constructor(private capsuleService: CapsuleService, private cd: ChangeDetectorRef) { }

  /**
   * Get addons
   * @param capsuleId capsule id got from component parameter
   */
  getAddons(capsuleId): void {
    this.capsuleService.getAddons(capsuleId, "accordion-item").subscribe(
      addons => this.addons = addons,
      () => { },
      () => {
        this.bindPath()
        this.cd.detectChanges()
      }
    );
  }

  /**
   * Get runtimes fam to show the correct images
   * @param runId list of ids
   */
  getRuntimesFam(runId: string[]) {
    runId.map((item, i) => {
      this.capsuleService.getRuntime(item, "accordion-item").subscribe(
        runtime => this.runtimeFam = runtime.fam,
        (err) => console.log(err),
        () => {
          this.addons[i].imgPath = this.setImg(this.runtimeFam)
        }
      )
    })
  }

  ngOnInit(): void {
    if (this.hasAddons) this.getAddons(this.capsuleId)
    this.freeDragPosition = JSON.parse(localStorage.getItem('AddonsCardPos'))
    if (this.freeDragPosition == undefined) this.freeDragPosition = { x: 0, y: 0 }
    if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
  }

  // ngOnChanges() {
  //   if (this.hasAddons) this.getAddons(this.capsuleId)
  // }

  // Detect if the capsule that got selected has addons 
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      let chng = changes[propName]
      if (!chng.firstChange) {
        if (chng.previousValue != chng.currentValue) {
          if (propName == "hasAddons" && chng.currentValue == true || this.hasAddons == true)
            this.getAddons(this.capsuleId)
          else this.addons = []
        }
      }
    }
  }

  /**
   * Stuff for drag functionnality
   */
  resetPosition() {
    this.freeDragPosition = { x: 0, y: 0 };
    this.resetDisabled = true
    localStorage.setItem('AddonsCardPos', JSON.stringify(this.freeDragPosition))
  }

  /**
   * Stuff for drag functionnality
   */
  afterDrag($ev: CdkDragEnd) {
    this.freeDragPosition = $ev.source.getFreeDragPosition()
    if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
    localStorage.setItem('AddonsCardPos', JSON.stringify(this.freeDragPosition))
  }

  /**
   * Get the runtime id of all the addons
   */
  bindPath() {
    const runIds: string[] = []
    if (this.addons != null) {
      this.addons.map(item => {
        runIds.push(item.runtime_id)
      })
      this.getRuntimesFam(runIds)
    }
  }

  /**
   * Set the path of the given family name
   * @param name runtimes family name
   */
  setImg(name: string) {
    switch (name.toLowerCase()) {
      case "mysql":
        return "../assets/square-mysql.svg"
      case "postgresql":
        return "../assets/square-psql.svg"
      case "redis":
        return "../assets/square-redis.svg"
      case "memcached":
        return "../assets/square-mem.svg"
      default:
        return "";
    }
  }

}
