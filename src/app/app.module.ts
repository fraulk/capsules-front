import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CookieService } from 'ngx-cookie-service';
import { KeycloakService, KeycloakAngularModule } from 'keycloak-angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbIconModule, NbSidebarModule, NbLayoutModule, NbButtonModule, NbSelectModule, NbListModule, NbActionsModule, NbCardModule, NbAccordionModule, NbMenuModule, NbInputModule, NbToastrModule, NbPopoverModule, NbToggleModule, NbSpinnerModule, NbDialogModule, NbStepperModule, NbTabsetModule, NbTooltipModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { CapsuleOverviewComponent } from './capsule-overview/capsule-overview.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';
import { WebappComponent } from './webapp/webapp.component';
import { AddCapsuleComponent } from './add-capsule/add-capsule.component';
import { CurrentWebappComponent } from './current-webapp/current-webapp.component';
import { InputFieldComponent } from './input-field/input-field.component';
import { RuntimesComponent } from './runtimes/runtimes.component';
import { AddAddonsComponent } from './add-addons/add-addons.component';
import { AddonsListComponent } from './addons-list/addons-list.component';
import { AddonInfoComponent } from './addon-info/addon-info.component';
import { AddonsChartComponent } from './addons-chart/addons-chart.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { WebappListComponent } from './webapp-list/webapp-list.component';
import { WebappCreationComponent } from './webapp-creation/webapp-creation.component';
import { AddonCreationComponent } from './addon-creation/addon-creation.component';
import { ApptokensComponent } from './apptokens/apptokens.component';
import { SshKeysComponent } from './ssh-keys/ssh-keys.component';
import { AppAuthGuard } from './app.authguard';
import { initializer } from './app-init';
import { KeycloakInterceptorService } from './keycloak.interceptor.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    CapsuleOverviewComponent,
    AccordionItemComponent,
    WebappComponent,
    AddCapsuleComponent,
    CurrentWebappComponent,
    InputFieldComponent,
    RuntimesComponent,
    AddAddonsComponent,
    AddonsListComponent,
    AddonInfoComponent,
    AddonsChartComponent,
    NotFoundComponent,
    WebappListComponent,
    WebappCreationComponent,
    AddonCreationComponent,
    ApptokensComponent,
    SshKeysComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }), // theme name
    NbLayoutModule,
    NbEvaIconsModule,
    RouterModule, // RouterModule.forRoot(routes, { useHash: true }), if this is your app.module
    NbMenuModule.forRoot(),
    NbMenuModule,
    NbSidebarModule.forRoot(),
    NbButtonModule,
    NbIconModule,
    NbSelectModule,
    NbListModule,
    NbActionsModule,
    NbCardModule,
    NbAccordionModule,
    NbInputModule,
    NbToastrModule.forRoot(),
    NbPopoverModule,
    NbToggleModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'en'
    }),
    NbSpinnerModule,
    FormsModule,
    NbDialogModule.forRoot(),
    NbStepperModule,
    DragDropModule,
    NbTabsetModule,
    NbTooltipModule,
    KeycloakAngularModule,
  ],
  providers: [
    CookieService,
    AppAuthGuard,
    // Comment/uncomment this part to enable keycloak auth on app initialisation >>>
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      deps: [KeycloakService],
      multi: true
    }, //<<<
    {
      provide: HTTP_INTERCEPTORS,
      useClass: KeycloakInterceptorService,
      multi: true
    },
    KeycloakService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
