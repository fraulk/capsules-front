import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApptokensComponent } from './apptokens.component';

describe('ApptokensComponent', () => {
  let component: ApptokensComponent;
  let fixture: ComponentFixture<ApptokensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApptokensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApptokensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
