import { Component, OnInit } from '@angular/core';
import { CapsuleService } from '../capsule.service';
import { NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-apptokens',
  templateUrl: './apptokens.component.html',
  styleUrls: ['./apptokens.component.scss']
})
export class ApptokensComponent implements OnInit {
  appTokens: any[] = []
  token = null
  isFetched: boolean = false
  isTokenReceived: boolean = false
  error: any; errorMsg1: any

  getApptokens() {
    this.capsuleService.getApptokens("apptokens").subscribe(
      (apptokens) => this.appTokens = apptokens,
      (err) => console.log(err),
      () => {
        if (this.appTokens != null) this.isFetched = true
      }
    )
  }

  /**
   * Post a new apptoken and add the post's response to the apptokens list
   * @param apptoken apptoken object
   */
  postApptokens(apptoken) {
    this.capsuleService.addApptoken(apptoken, "apptokens").subscribe(
      (token) => this.token = token,
      (err) => console.log(err),
      () => {
        if (this.token != null) {
          this.isTokenReceived = true
          this.isFetched = true
          if (this.appTokens == null) this.appTokens = [this.token]
          else this.appTokens.push(this.token)
        }
      }
    )
  }

  deleteApptoken(id) {
    this.capsuleService.deleteApptoken(id, "apptokens").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.getApptokens()
      }
    )
  }

  constructor(private capsuleService: CapsuleService, private toastrService: NbToastrService, public translate: TranslateService, private titleService: Title) { }

  onSend(app) {
    let inputControl = app.match(/^[a-z0-9]{5,20}$/i)
    if (inputControl == null) {
      this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' });
      return
    }
    let apptoken = {
      "app": app
    }
    this.postApptokens(apptoken)
  }

  delete(id) {
    this.deleteApptoken(id)
  }

  /**
   * Copy the input content into the user's clipboard
   * @param inputElement The input field to copy (can be hidden)
   */
  copy(inputElement) {
    inputElement.select()
    document.execCommand('copy')
    inputElement.setSelectionRange(0, -1)
  }

  close() { this.isTokenReceived = false }

  ngOnInit(): void {
    this.getApptokens()
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validAppname').subscribe(res => this.errorMsg1 = res)
    this.titleService.setTitle("Capsule Front - App Tokens")
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validAppname').subscribe(res => this.errorMsg1 = res)
    });
  }

}
