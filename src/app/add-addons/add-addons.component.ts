import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router"
import { Addons } from '../objects/addons';
import { NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { CapsuleService } from '../capsule.service';

@Component({
  selector: 'app-add-addons',
  templateUrl: './add-addons.component.html',
  styleUrls: ['./add-addons.component.scss']
})

export class AddAddonsComponent implements OnInit {
  @Input() capsuleId: string
  @Output() needRefresh = new EventEmitter()
  error: string = ""
  errorMsg2: string = ""; errorMsg3: string = ""
  success: string = ""; successMsg: string = ""
  nbCardAccent: string = "primary"

  constructor(private toastrService: NbToastrService, public translate: TranslateService, private router: Router, private capsuleService: CapsuleService) { }

  /**
   * Post a new addon, then navigate to overview and refresh
   * @param newAddon Addon object to post
   */
  addAddon(newAddon): void {
    this.capsuleService.addAddon(newAddon, this.capsuleId, "add-addons").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        setTimeout(() => {
          this.needRefresh.emit()
          setTimeout(() => {
            this.router.navigate(['/overview'])
          }, 500);
        }, 500);
      }
    )
  }

  ngOnInit(): void {
    this.translate.get('ERROR.error').subscribe(res => this.error = res);
    this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
    this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg3 = res);
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res);
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res);
  }

  //removed stuff to be able to add multiple addons at once
  // onSave(name: listOfAddons[]) {
  //   let addonsParam: number = 0, addonsList: number = 0
  //   name.map(() => addonsParam++)
  //   this.listOfNewAddon.map(() => addonsList++)
  //   console.log(this.listOfNewAddon, addonsList, addonsParam)
  //   if (addonsParam > addonsList || addonsParam > 0 && addonsList == 0) {
  //     this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' })
  //     this.nbCardAccent = "danger"
  //     return
  //   } else if (addonsParam == 0 && addonsList == 0) {
  //     this.toastrService.show(this.errorMsg2, this.error, { status: 'danger' })
  //     this.nbCardAccent = "danger"
  //     return
  //   }
  //   this.toastrService.show(this.successMsg, this.success, { status: 'success' })
  //   console.log("post")
  // }

  // onAddonsListChange(evt: any) {
  //   console.log(evt.value)
  //   if (this.listOfNewAddon.some(e => e.id === evt.value)) this.listOfNewAddon.splice(this.listOfNewAddon.map((item) => { return item.id }).indexOf(evt.value), 1)
  //   this.nbCardAccent = "warning"
  //   console.log(this.listOfNewAddon)
  // }

  add(newAddon) {
    this.addAddon(newAddon)
    this.nbCardAccent = "success"
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res);
      this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
      this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg3 = res);
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res);
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res);
    });
  }

}
