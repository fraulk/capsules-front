import { Component, OnInit, Output, EventEmitter, Host, Inject, forwardRef, ViewChild } from '@angular/core';
import { NbToastrService, NbSidebarService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { CapsuleService } from '../capsule.service';
import { AppComponent } from '../app.component';
import { WebappCreationComponent } from '../webapp-creation/webapp-creation.component';
import { AddonCreationComponent } from '../addon-creation/addon-creation.component';

@Component({
  selector: 'app-add-capsule',
  templateUrl: './add-capsule.component.html',
  styleUrls: ['./add-capsule.component.scss']
})
export class AddCapsuleComponent implements OnInit {
  @ViewChild(WebappCreationComponent) webappCreationComp: WebappCreationComponent
  @ViewChild(AddonCreationComponent) addonCreationComp: AddonCreationComponent
  helpMsg = "Appuyez sur la touche Entrée pour ajouter l'utilisateur"
  users = [];
  webappsImgPaths = [
    "../assets/square-php-apache.svg",
    "../assets/square-nodejs.svg",
    "../assets/square-ruby.svg",
  ]
  error: string
  errorMsg: string; errorMsg2: any; errorMsg3: any; errorMsg4: any;
  success: string
  successMsg: string
  nbCardAccent: string = "primary"
  webappNo: number
  webapp: any
  addon: any
  capsuleId: string

  constructor(private toastrService: NbToastrService, public translate: TranslateService, private router: Router, private capsuleService: CapsuleService, @Inject(forwardRef(() => AppComponent)) private _parent: AppComponent, private sidebarService: NbSidebarService) { }

  /**
   * Post a capsule then post the webapp if it exist, and the addon if it exist
   * @param newCapsule The capsule object to add
   */
  addCapsule(newCapsule) {
    this.capsuleService.addCapsule(newCapsule, "add-capsule")
      .subscribe(
        capsule => this.capsuleId = capsule.id, // console.log(capsule)
        (err) => console.log(err),
        () => {
          console.log(this.webapp)
          console.log(this.addon)
          if (this.webapp != undefined || this.webapp != null)
            setTimeout(() => {
              this.addWebapp(this.webapp)
            }, 500)
          if (this.addon != undefined || this.addon != null)
            setTimeout(() => {
              this.addAddon(this.addon)
            }, 900)
          if (this.webapp == undefined && this.addon == undefined)
            this._parent.getCapsules()
        } //had to inject instead of using Output/EventEmitter because of the router outlet, it has no parent
      );
  }

  /**
   * Post a webapp, then refresh
   * @param newWebapp The webapp object to add
   */
  addWebapp(newWebapp) {
    this.capsuleService.addWebapp(newWebapp, this.capsuleId, "add-capsule")
      .subscribe(
        webapp => { },
        (err) => console.log(err),
        () => {
          this._parent.getCapsules()
        }
      )
  }

  /**
   * Post an addon, then refresh
   * @param newAddon The addon object to add
   */
  addAddon(newAddon) {
    this.capsuleService.addAddon(newAddon, this.capsuleId, "add-capsule")
      .subscribe(
        addon => { },
        (err) => console.log(err),
        () => {
          this._parent.getCapsules()
        }
      )
  }

  /**
   * Check if inputs are correct, then create a webapp/addons using their creation component, and finally creates the capsule
   * @param name capsule's name
   * @param users users linked to the capsules
   * @param addons addons added to the capsule
   * @param webappName selected webapp name
   */
  onSave(name: string, users: string[], addons: any[], webappName: string) {
    if (name == "" || name == undefined) {
      this.toastrService.show(this.errorMsg, this.error, { status: 'danger' });
      this.nbCardAccent = "danger"
      return
    }
    let inptControl = (name.match(/[a-z0-9\-_]+/m) != null) ? name.match(/[a-z0-9\-_]+/m)[0] : ""
    if (name != inptControl) {
      this.toastrService.show(this.errorMsg3 + " " + this.errorMsg2, this.error, { status: 'danger' });
      return
    }
    if (users.length == 0) {
      this.toastrService.show(this.errorMsg4, this.error, { status: 'danger' });
      return
    }
    let newCapsule = {
      "name": name,
      "owners": users
    }
    if (webappName != undefined) {
      this.webappCreationComp.add(webappName, this.webappCreationComp.fqdn, this.webappCreationComp.alias)
      this.webapp = this.webappCreationComp.finalWebapp
      if (this.webappCreationComp.isIncorrect) return
    }
    if (addons.length != 0) {
      this.addonCreationComp.add(this.addonCreationComp.name, this.addonCreationComp.desc, addons[0].name)
      this.addon = this.addonCreationComp.finalAddon
      if (this.addonCreationComp.isIncorrect) return
    }
    this.nbCardAccent = "success"
    this.sidebarService.expand('left')
    sessionStorage.setItem("capsuleName", newCapsule.name)
    this.addCapsule(newCapsule)
    setTimeout(() => {
      this.router.navigate(['/'])
    }, 500);
    console.log("post : " + name)
  }

  /**
   * Tries to reset the select when a new webapp/addon got selected (not working, idk why)
   * @param select Select html element
   */
  onSelectChoice(select) {
    if (select != undefined) select.reset()
  }

  /**Removes the users in the list in first step */
  removeUser(values: string[], i: number) {
    values.splice(i, 1)
  }

  ngOnInit(): void {
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validName').subscribe(res => this.errorMsg = res)
    this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
    this.translate.get('ADDCAPS.nameField').subscribe(res => this.errorMsg3 = res);
    this.translate.get('ADDCAPS.userField').subscribe(res => this.errorMsg4 = res);
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validName').subscribe(res => this.errorMsg = res)
      this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
      this.translate.get('ADDCAPS.nameField').subscribe(res => this.errorMsg3 = res);
      this.translate.get('ADDCAPS.userField').subscribe(res => this.errorMsg4 = res);
    });
    setTimeout(() => {
      this.sidebarService.collapse('left')
    });
  }

}
