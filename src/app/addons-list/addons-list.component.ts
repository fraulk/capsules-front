import { Component, OnInit, EventEmitter, Output, ElementRef, ChangeDetectorRef, Input } from '@angular/core';
import { CapsuleService } from '../capsule.service';

@Component({
  selector: 'app-addons-list',
  templateUrl: './addons-list.component.html',
  styleUrls: ['./addons-list.component.scss']
})
export class AddonsListComponent implements OnInit {
  @Output() choice: EventEmitter<any> = new EventEmitter<boolean>()
  selectedAddons: any[] = []
  clicked: number = 0
  oldName: string = ""
  runtimes: any[]
  imgsPath: string[] = []
  selectedFamily: any[];

  constructor(private capsuleService: CapsuleService, private cd: ChangeDetectorRef) { }

  /**
   * Get runtimes info and sets the images path
   */
  getRuntimes(): void {
    this.capsuleService.getRuntimesAddons("addon list")
      .subscribe(
        runtimes => this.runtimes = runtimes,
        (err) => console.log(err),
        () => {
          if (this.runtimes == null) return
          this.setImages()
          this.cd.detectChanges()
        }
      );
  }

  ngOnInit(): void {
    this.getRuntimes()
  }

  /**
   * Selects the clicked addon
   * @param name addon's name
   * @param id addon's id
   * @param addon addon html element (checkbox)
   */
  onAddonSelect(name: string, id: number, addon) {
    name = name.match(/-(.*)\./m)[1] // ex : Array [ "-redis.", "redis" ]
    name = (name == "psql") ? "postgresql" : name
    this.clicked++
    if (addon.checked == true && this.clicked >= 2 && this.oldName == name) {
      addon.checked = false
      this.clicked = 0
      this.selectedAddons = []
      this.selectedFamily = []
      return
    }
    let family = []
    this.runtimes.map(item => {
      if (item.fam.toLowerCase().indexOf(name) >= 0)
        family.push(item)
    })
    this.selectedFamily = family
    this.oldName = name
    this.selectedAddons = []
    this.selectedAddons.push({ name: name, id: id })
    this.choice.emit(true)
  }

  setImages() {
    let family = []
    this.runtimes.map(item => {
      family.push(item.fam)
    })
    family = [...new Set(family)]; // to remove duplicates
    family.map(item => {
      switch (item.toLowerCase()) {
        case "mysql":
          this.imgsPath.push("../assets/square-mysql.svg")
          break;

        case "postgresql":
          this.imgsPath.push("../assets/square-psql.svg")
          break;

        case "redis":
          this.imgsPath.push("../assets/square-redis.svg")
          break;

        case "memcached":
          this.imgsPath.push("../assets/square-mem.svg")
          break;

        default:
          break;
      }
    })
  }

}
