import { Component, OnInit, Input } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { CapsuleService } from '../capsule.service';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent implements OnInit {
  @Input() labelText: any;
  @Input() single: false;
  @Input() user: false
  values: string[] = []
  value: string
  error: string
  errorMsg1: string; errorMsg2: string; errorMsg3: string
  success: string
  successMsg: string
  currentUser: string;

  getUser(name: string) {
    this.capsuleService.getUserByName(name, "add-capsule").subscribe(
      (user) => this.currentUser = user,
      (err) => console.log(err),
      () => {
        if (this.currentUser == null) {
          this.values.pop()
          this.toastrService.show(this.errorMsg3, this.error, { status: 'danger' });
        } else
          this.toastrService.show(this.successMsg, this.success, { status: 'success' });
      }
    )
  }

  constructor(private toastrService: NbToastrService, public translate: TranslateService, private capsuleService: CapsuleService) { }

  /**
   * Add the element to the array, after checking if the user exist if user component param is true
   * @param value input value
   */
  onEnter(value: string) {
    if (this.user)
      this.getUser(value)
    if (value) {
      this.values.push(value)
    } else {
      this.toastrService.show(this.errorMsg2, this.error, { status: 'danger' });
    }
  }

  onValueEnter(value: string) {
    if (!value) {
      this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' });
    }
    this.value = value
  }

  ngOnInit(): void {
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validName').subscribe(res => this.errorMsg1 = res)
    this.translate.get('ERROR.validElement').subscribe(res => this.errorMsg2 = res)
    this.translate.get('ERROR.userDoesntExist').subscribe(res => this.errorMsg3 = res)
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validName').subscribe(res => this.errorMsg1 = res)
      this.translate.get('ERROR.validElement').subscribe(res => this.errorMsg2 = res)
      this.translate.get('ERROR.userDoesntExist').subscribe(res => this.errorMsg3 = res)
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
    });
  }

}
