import { Component, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { NbSidebarService, NbThemeService, NbSelectComponent } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Capsule } from '../app/objects/capsule'
import { CapsuleService } from './capsule.service';
import { Webapp } from './objects/webapp';
import { CookieService } from 'ngx-cookie-service';
import { CurrentWebappComponent } from './current-webapp/current-webapp.component';
import { CapsuleOverviewComponent } from './capsule-overview/capsule-overview.component';
import { AccordionItemComponent } from './accordion-item/accordion-item.component';
import { environment } from 'src/environments/environment';
import { KeycloakService } from 'keycloak-angular';
import { WebappComponent } from './webapp/webapp.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'capsule-front';
  selectedCapsule: any
  capsules: Capsule[]
  webapp: Webapp
  // items and otherItems are for the top and bottom menu, they're almost identical except for the hidden attribute, this way it makes the selected item unique instead of being able to select 2 items from 2 menus
  items = [
    {
      title: 'Overview',
      icon: 'home-outline',
      link: '/overview',
      pathMatch: 'full',
    },
    {
      title: 'Webapp',
      icon: 'globe-outline',
      link: '/webapp',
      pathMatch: 'full',
    },
    {
      title: 'Addons',
      icon: { icon: 'settings-2-outline', pack: 'eva' },
      children: [{
        title: "Ajouter un addon",
        icon: { icon: 'plus-outline', pack: 'eva' },
        link: '/add-addons',
        queryParams: null,
      }],
      pathMatch: 'full',
    },
    {
      title: 'Runtimes',
      icon: 'activity-outline',
      link: '/runtimes',
      hidden: true,
    },
    {
      title: 'App Tokens',
      icon: 'stop-circle-outline',
      link: '/apptokens',
      hidden: true,
    },
    {
      title: 'SSH Keys',
      icon: 'link-2-outline',
      link: '/ssh-keys',
      hidden: true,
    }
  ];
  otherItems = [
    {
      title: 'Overview',
      icon: 'home-outline',
      link: '/overview',
      hidden: true,
    },
    {
      title: 'Webapp',
      icon: 'globe-outline',
      link: '/webapp',
      hidden: true,
    },
    {
      title: 'Addons',
      icon: { icon: 'settings-2-outline', pack: 'eva' },
      children: [{
        title: "Ajouter un addon",
        icon: { icon: 'plus-outline', pack: 'eva' },
        link: '/add-addons',
        queryParams: null,
      },
      {
        title: "null",
        icon: { icon: 'plus-outline', pack: 'eva' },
        link: '/addon',
        queryParams: null,
      }],
      hidden: true,
    },
    {
      title: 'Runtimes',
      icon: 'activity-outline',
      link: '/runtimes',
      pathMatch: 'prefix',
    },
    {
      title: 'App Tokens',
      icon: 'stop-circle-outline',
      link: '/apptokens',
      pathMatch: 'prefix',
    },
    {
      title: 'SSH Keys',
      icon: 'link-2-outline',
      link: '/ssh-keys',
      pathMatch: 'prefix',
    }
  ]
  isLoading: Boolean = true
  langs = this.translate.getLangs()
  addAddon: string = ""
  addonStatus: string = "id"
  themeList: string[] = [
    "default",
    "dark",
    "cosmic",
    "aquamarine",
    "highcontrast"
  ]
  selectedTheme: string = ""
  isAPIworking: boolean = true
  refreshLocked: boolean = false
  firstCapsule: string = "0"
  resizeCount: number = 0
  plusDisabled: boolean = false
  minusDisabled: boolean = false
  resetDisabled: boolean = true
  hasWebapp: boolean = true
  hasAddons: boolean = true
  isCompact: boolean = false
  isCompactCheck: boolean = false
  isAnim: boolean = true
  isAnimCheck: boolean = true
  role: string = 'user'
  @ViewChild(CurrentWebappComponent) currWebapp
  @ViewChild(CapsuleOverviewComponent) capsOvrvw
  @ViewChild(AccordionItemComponent) accordionItem
  @ViewChild(WebappComponent) webappCpnt
  @ViewChild('bodyLayout', { read: ElementRef }) bodyLayout: ElementRef;
  @ViewChild('selectCaps') selectCapsElement

  /**
   * Gets a list of capsules, then auto select the first capsule, detect if the selected capsule has webapp/addons
   */
  getCapsules(): void {
    this.capsuleService.getCapsules()
      .subscribe(
        capsules => this.capsules = capsules,
        (err) => {
          console.log(err)
          this.isLoading = false
        },
        () => {
          if (this.capsules == null) {
            this.isLoading = false
            this.isAPIworking = false
            return
          } else {
            let prevCapsuleName = sessionStorage.getItem("capsuleName")
            this.capsules.map((item, i) => {
              if (item.name == prevCapsuleName)
                this.selectedCapsule = item
            })
            if (this.selectedCapsule == undefined) this.selectedCapsule = this.capsules[0]
            if (this.selectedCapsule.webapp == null || this.selectedCapsule.webapp == "") this.hasWebapp = false; else this.hasWebapp = true
            if (this.selectedCapsule.addons.length == 0) this.hasAddons = false; else this.hasAddons = true
            if (this.hasAddons) this.getAddonsByCapsuleId()
            sessionStorage.setItem("capsuleName", this.selectedCapsule.name)
            this.firstCapsule = this.selectedCapsule.name
            setTimeout(() => {
              this.selectCapsElement.setSelection(this.firstCapsule)
            }, 500);
            this.onSelect(this.selectedCapsule)
            this.isLoading = false
          }
        }
      );
  }

  /**
   * Gets all the addons of one capsule to populate the addons menu on the left sidebar
   */
  getAddonsByCapsuleId(): void {
    if (this.selectedCapsule == undefined) return
    this.capsuleService.getAddons(this.selectedCapsule.id, "app (root)")
      .subscribe(
        addons => this.selectedCapsule.addons = addons,
        (err) => console.log(err),
        () => {
          if (this.selectedCapsule.addons == null) this.selectedCapsule.addons = []
          // populate sidebar addons name
          this.populateSidebarAddons()
          this.addonStatus = "data"
          this.isLoading = false
        }
      )
  }

  /**
   * Gets the data of the current user and checks the role to adjust his rights
   */
  getUserRights() {
    this.capsuleService.getCurrentUser("app (root)").subscribe(
      (user) => this.role = (user != null) ? user.role : "",
      (err) => console.log(err),
      () => {
        console.log(this.role)
        sessionStorage.setItem("role", this.role)
      }
    )
  }

  /**
   * Event fired when user select capsule, swith the current capsule for the new one and checks for webapp/addons
   * @param capsule The selected capsule object
   */
  onSelect(capsule: Capsule): void {
    this.selectedCapsule = capsule;
    if (this.selectedCapsule.webapp == null || this.selectedCapsule.webapp == "") this.hasWebapp = false
    else this.hasWebapp = true
    if (this.selectedCapsule.addons.length == 0) this.hasAddons = false
    else this.hasAddons = true
    if (this.hasAddons) this.getAddonsByCapsuleId()
    this.router.navigate(['/overview'])
    sessionStorage.setItem("capsuleName", this.selectedCapsule.name)
    // populate sidebar addons name at capsule change
    this.populateSidebarAddons()
  }

  /**
   * Populate addons on the left sidebar
   */
  populateSidebarAddons() {
    if (this.selectedCapsule === undefined) return
    this.items[2].children = []
    this.selectedCapsule.addons.map((item, i) => {
      this.items[2].children[i] = {
        title: item.name,
        icon: { icon: 'settings-2-outline', pack: 'eva' },
        link: '/addon',
        queryParams: { name: item.name }
      }
    })
    this.translate.get('HOME.addAddon').subscribe((res: string) => {
      this.addAddon = res
    });
    this.items[2].children[this.items[2].children.length] = {
      title: this.addAddon,
      icon: { icon: 'plus-outline', pack: 'eva' },
      link: '/add-addons',
      queryParams: null
    }
  }

  /**
   * Change the current language
   * @param lang en/fr
   */
  translateIt(lang: string) {
    this.translate.use(lang)
    this.cookieService.set('lang', lang)
  }

  /**
   * Change the current theme
   * @param themeName theme name
   */
  themeChange(themeName: string) {
    this.themeService.changeTheme(themeName)
    this.cookieService.set('theme', themeName)
  }

  /**
   * Toggle customization sidebar
   */
  toggleRight() {
    this.sidebarService.toggle(false, 'right')
  }

  /**
   * Refresh the data by requesting capsules (locked 5 seconds after one click)
   * @param selectEl the capsule select html element
   */
  refresh(selectEl) {
    if (!this.refreshLocked) {
      this.refreshLocked = true
      setTimeout(() => {
        this.refreshLocked = false
      }, 5000);
      this.getCapsules()
      this.firstCapsule = sessionStorage.getItem("capsuleName")
      console.log(this.firstCapsule)
      selectEl.setSelection(this.firstCapsule)
      if (this.router.url == "/webapp" && (this.selectedCapsule.id != undefined || this.selectedCapsule.id != null))
        this.webappCpnt.getWebapp(this.selectedCapsule.id)
    }
  }

  prevSizeNum = 1
  /**
   * Make the app size bigger or smaller or reset it
   * @param sign + or - or reset
   */
  resizeText(sign: string) {
    let maxSize = 5; let minSize = -3
    switch (sign) {
      case "+":
        if (this.resizeCount >= maxSize - 1) this.plusDisabled = true
        if (this.minusDisabled) this.minusDisabled = false
        if (this.resetDisabled) this.resetDisabled = false
        if (this.resizeCount >= maxSize) return
        this.prevSizeNum += 0.1
        document.documentElement.style.setProperty('font-size', this.prevSizeNum + "rem")
        break;

      case "-":
        if (this.resizeCount <= minSize + 1) this.minusDisabled = true
        if (this.plusDisabled) this.plusDisabled = false
        if (this.resetDisabled) this.resetDisabled = false
        if (this.resizeCount <= minSize) return
        this.prevSizeNum -= 0.1
        document.documentElement.style.setProperty('font-size', this.prevSizeNum + "rem")
        break;

      case "reset":
        if (this.minusDisabled) this.minusDisabled = false
        if (this.plusDisabled) this.plusDisabled = false
        if (!this.resetDisabled) this.resetDisabled = true
        this.prevSizeNum = 1
        document.documentElement.style.setProperty('font-size', + this.prevSizeNum + "rem")
        break

      default:
        break;
    }
    this.resizeCount = (sign == "+") ? this.resizeCount + 1 : (sign == "-") ? this.resizeCount - 1 : 0
    if (this.resizeCount == 0) this.resetDisabled = true
  }

  /**
   * Reset the position of cards in the overview
   */
  resetCards() {
    if (!this.capsOvrvw || !this.currWebapp || !this.accordionItem) return
    this.capsOvrvw.resetPosition()
    this.currWebapp.resetPosition()
    this.accordionItem.resetPosition()
  }

  /**
   * Specific refresh for webapps
   * @param selectEl Capsule select html element
   * @param event Data passed from the event
   */
  handleRefresh(selectEl, event = null) {
    setTimeout(() => {
      this.getCapsules()
    }, 1000);
    if (event != null && event == "hasWebappFalse")
      this.hasWebapp = false
    if (event != null && event == "hasWebappTrue")
      this.hasWebapp = true
    console.log(event)
    console.log(this.hasWebapp)
    this.onSelect(this.selectedCapsule)
    // selectEl.setSelection(this.firstCapsule)
  }

  /**
   * Specific refresh for addons
   * @param selectEl Capsule select html element
   * @param event Data passed from the event
   */
  handleRefreshNotW(selectEl, event = null) {
    console.log(event)
    this.getCapsules()
    this.onSelect(this.capsules[0])
    selectEl.setSelection(this.capsules[0].name)
    setTimeout(() => {
      this.getAddonsByCapsuleId()
      this.accordionItem.getAddons(this.selectedCapsule.id)
    }, 1000);
    // selectEl.setSelection(this.firstCapsule)
  }

  /**
   * Customization option that makes the left sidebar compact
   */
  compactSidebar() {
    this.isCompact = !this.isCompact
    if (this.isCompact)
      this.sidebarService.compact('left')
    else
      this.sidebarService.expand('left')
    this.cookieService.set('compact', `${this.isCompact}`)
  }

  /**
   * Customization option that disable/enable animations
   */
  toggleAnimations() {
    this.isAnim = !this.isAnim
    if (this.isAnim)
      this.bodyLayout.nativeElement.classList.remove("noTransition")
    else
      this.bodyLayout.nativeElement.classList.add("noTransition")
    this.cookieService.set('animToggle', `${this.isAnim}`)
  }

  ngOnInit() {
    this.getCapsules()
    this.getUserRights()
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('HOME.addAddon').subscribe((res: string) => {
        this.addAddon = res
        this.populateSidebarAddons()
      });
    });
  }

  constructor(private sidebarService: NbSidebarService, public themeService: NbThemeService, public router: Router, private capsuleService: CapsuleService, private cd: ChangeDetectorRef, public translate: TranslateService, private cookieService: CookieService, public breakpointObserver: BreakpointObserver, private keycloakService: KeycloakService) {
    translate.addLangs(['en', 'fr']);
    const browserLang = translate.getBrowserLang();
    translate.setDefaultLang('en')
    if (cookieService.check('lang')) translate.use(cookieService.get('lang'))
    else translate.use(browserLang)
    if (cookieService.check('theme')) {
      themeService.changeTheme(cookieService.get('theme'))
      this.selectedTheme = cookieService.get('theme')
    }
    if (cookieService.check('compact')) {
      this.isCompact = (cookieService.get('compact') == "true") ? true : false
      this.isCompactCheck = (this.isCompact) ? true : false
    }
    if (cookieService.check('animToggle')) {
      this.isAnim = (cookieService.get('animToggle') == "true") ? true : false
      this.isAnimCheck = (this.isAnim) ? true : false
    }
  }

  toggle() {
    this.sidebarService.toggle(this.isCompact, 'left');
  }
}
