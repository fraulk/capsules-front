import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { CapsuleService } from '../capsule.service';

@Component({
  selector: 'app-webapp-list',
  templateUrl: './webapp-list.component.html',
  styleUrls: ['./webapp-list.component.scss']
})
export class WebappListComponent implements OnInit {
  @Output() choice: EventEmitter<any> = new EventEmitter<string>()
  @Output() htmlEl: EventEmitter<any> = new EventEmitter<string>()
  imgsPath: string[] = []
  runtimes: any[]
  selectedFamily: any[] = [""]
  oldName: string = ""
  clicked: number = 0

  constructor(private cd: ChangeDetectorRef, private capsuleService: CapsuleService) { }

  /**
   * Add selected webapp to an array
   * @param name family name
   * @param htelm checkbox html element
   */
  add(name, htelm) {
    name = name.match(/-(.*)\./m)[1] //; console.log(name)
    name = (name == "php-apache") ? "php" : name // to get rid of "-apache" in the name of the php image
    this.clicked++
    if (htelm.checked == true && this.clicked >= 2 && this.oldName == name) {
      htelm.checked = false
      this.clicked = 0
      this.selectedFamily = [""]
      return
    }
    let family = []
    this.runtimes.map(item => { // sorting runtimes by family selected
      if (item.fam.toLowerCase().indexOf(name) >= 0)
        family.push(item)
    })
    this.oldName = name
    this.selectedFamily = family
    this.choice.emit(name)
    this.htmlEl.emit({ name: name, htmlElement: htelm })
  }

  ngOnInit(): void {
    this.getRuntimes()
  }

  getRuntimes(): void {
    this.capsuleService.getRuntimesWebapps("webapp list")
      .subscribe(
        runtimes => this.runtimes = runtimes,
        (err) => console.log(err),
        () => {
          if (this.runtimes == null) return
          this.setImages()
          this.cd.detectChanges()
        }
      );
  }

  setImages() {
    let family = []
    this.runtimes.map(item => {
      family.push(item.fam)
    })
    family = [...new Set(family)]; // to remove duplicates
    family.map(item => {
      switch (item) {
        case "Apache PHP":
          this.imgsPath.push("../assets/square-php-apache.svg")
          break;

        case "NodeJS":
          this.imgsPath.push("../assets/square-nodejs.svg")
          break;

        case "Ruby":
          this.imgsPath.push("../assets/square-ruby.svg")
          break;

        default:
          break;
      }
    })
  }

}
