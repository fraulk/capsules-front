import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Capsule } from './objects/capsule';
import { Observable, of, from } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Webapp } from './objects/webapp';
import { Addons } from './objects/addons';

@Injectable({
  providedIn: 'root'
})
export class CapsuleService {
  private baseUrl = environment.apiUrl
  private capsulesUrl = this.baseUrl + '/capsules'
  private runtimesUrl = this.baseUrl + '/runtimes'
  private apptpkensUrl = this.baseUrl + '/apptokens'
  private sshUrl = this.baseUrl + '/sshkeys'
  private userUrl = this.baseUrl + '/users'

  /**
   * The header gets the Authorization: Bearer in the http interceptor (./keycloak.interceptor.service.ts)
   */
  headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  }

  requestOptions = {
    mode: 'cors',
    method: 'GET',
    headers: new HttpHeaders(this.headerDict),
    redirect: 'follow'
  };

  constructor(private http: HttpClient) { }

  // GET capsules/
  getCapsules(): Observable<Capsule[]> {
    return this.http.get<Capsule[]>(this.capsulesUrl, this.requestOptions)
      .pipe(
        tap(_ => console.log('fetched capsules')),
        catchError(this.handleError<Capsule[]>('getCapsules', []))
      );
  }

  /** GET capsule/{id}. Will 404 if id not found */
  getCapsule(id: string): Observable<Capsule> {
    const url = `${this.capsulesUrl}/${id}`;
    return this.http.get<Capsule>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched capsule id=${id}`)),
      catchError(this.handleError<Capsule>(`getCapsule id=${id}`))
    );
  }

  // POST capsules/
  addCapsule(capsule: object, from: string = ""): Observable<any> {
    return this.http.post<Capsule>(this.capsulesUrl, capsule, this.requestOptions)
      .pipe(
        tap(_ => console.log(`posted capsules for ${from} at ${this.capsulesUrl}`)),
        catchError(this.handleError('addCapsule', capsule))
      );
  }

  // DELETE capsules/{id}
  deleteCapsule(id: string, from: string = ""): Observable<any[]> {
    const url = `${this.capsulesUrl}/${id}`;
    return this.http.delete(url, this.requestOptions)
      .pipe(
        tap(_ => console.log(`deleted capsules for ${from} at ${url}`)),
        catchError(this.handleError('deleteCapsule', []))
      );
  }

  // PATCH capsules/{id}
  patchUserByCapsule(capsuleId: string, owner: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/owners`
    return this.http.patch(url, owner, this.requestOptions).pipe(
      tap(_ => console.log(`patched owner to capsule for ${from} capsuleId=${capsuleId}`)),
      catchError(this.handleError('patchUserByCapsule'))
    )
  }

  // DELETE capsules/{id}/owner/{name}
  deleteUserByCapsule(capsuleId: string, name: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/owners/${name}`
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted owner to capsule for ${from} capsuleId=${capsuleId}`)),
      catchError(this.handleError('deleteUserByCapsule'))
    )
  }

  // GET capsules/{id}/webapp
  getWebapp(id: string, from: string = ""): Observable<Webapp> {
    const url = `${this.capsulesUrl}/${id}/webapp`;
    return this.http.get<Webapp>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched webapps id=${id} for ${from} at ${url}`)),
      catchError(this.handleError<Webapp>(`getWebapp from=${from} id=${id}`))
    );
  }

  // POST capsules/{id}/webapp
  addWebapp(webapp: object, capsuleId: string, from: string = ""): Observable<any[]> {
    const url = `${this.capsulesUrl}/${capsuleId}/webapp`
    return this.http.post<any>(url, webapp, this.requestOptions).pipe(
      tap(_ => console.log(`posted webapp for ${from} at ${url}`)),
      catchError(this.handleError('addWebapp', webapp))
    );
  }

  // PUT capsules/{id}/webapp
  putWebapp(webapp: object, capsuleId: string, from: string = ""): Observable<any[]> {
    const url = `${this.capsulesUrl}/${capsuleId}/webapp`
    return this.http.put<any>(url, webapp, this.requestOptions).pipe(
      tap(_ => console.log(`put webapp for ${from} at ${url}`)),
      catchError(this.handleError('putWebapp', webapp))
    );
  }

  // DELETE capsules/{id}/webapp
  deleteWebapp(id: string, from: string = ""): Observable<any[]> {
    const url = `${this.capsulesUrl}/${id}/webapp`;
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted webapp for ${from} at ${url}`)),
      catchError(this.handleError('deleteWebapp', []))
    );
  }

  // GET capsules/{id}/addons
  getAddons(id: string, from: string = ""): Observable<Addons[]> {
    const url = `${this.capsulesUrl}/${id}/addons`;
    return this.http.get<Addons[]>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched addons id=${id} for ${from} at ${url}`)),
      catchError(this.handleError<Addons[]>(`getAddons from ${from} id=${id}`))
    );
  }

  // POST capsules/{id}/addons
  addAddon(addon: object, id: string, from: string = "") {
    const url = `${this.capsulesUrl}/${id}/addons`;
    return this.http.post<Addons[]>(url, addon, this.requestOptions).pipe(
      tap(_ => console.log(`posted addon for ${from} at ${url}`)),
      catchError(this.handleError<Addons[]>(`addAddons`))
    );
  }

  // PUT capsules/{id}/addons/{id}
  putAddon(capsuleId: string, addonId: string, addon: object, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/addons/${addonId}`;
    return this.http.put(url, addon, this.requestOptions).pipe(
      tap(_ => console.log(`put addon for ${from} at ${url}`)),
      catchError(this.handleError('putAddon', []))
    )
  }

  // DELETE capsules/{id}/addons/{id}
  deleteAddon(capsuleId: string, addonId: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/addons/${addonId}`;
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted addon id="${addonId}" for capsule ${capsuleId} from ${from} at ${url}`)),
      catchError(this.handleError('deleteAddon', []))
    );
  }

  // GET runtimes/
  getRuntimes(from: string = ""): Observable<any[]> {
    const url = `${this.runtimesUrl}`;
    return this.http.get<any[]>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched runtimes for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getRuntimes`))
    );
  }

  // GET runtimes/{id}
  getRuntime(id: string, from: string = ""): Observable<any> {
    const url = `${this.runtimesUrl}/${id}`;
    return this.http.get<any[]>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched runtime id=${id} for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getRuntime`))
    );
  }

  // GET runtimes?filters[runtime_type]=webapp
  getRuntimesWebapps(from: string = ""): Observable<any[]> {
    const url = `${this.runtimesUrl}?filters[runtime_type]=webapp`;
    return this.http.get<any[]>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched runtimes (webapps) for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getRuntimes`))
    );
  }

  // GET runtimes?filters[runtime_type]=addons
  getRuntimesAddons(from: string = ""): Observable<any[]> {
    const url = `${this.runtimesUrl}?filters[runtime_type]=addon`;
    return this.http.get<any[]>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched runtimes (addons) for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getRuntimes`))
    );
  }

  // GET apptokens/
  getApptokens(from: string = ""): Observable<any[]> {
    return this.http.get<any[]>(this.apptpkensUrl, this.requestOptions).pipe(
      tap(_ => console.log(`fetched apptokens for ${from} at ${this.apptpkensUrl}`)),
      catchError(this.handleError<any[]>(`getApptokens`))
    );
  }

  // POST apptokens/
  addApptoken(token, from: string = ""): Observable<any> {
    return this.http.post<any>(this.apptpkensUrl, token, this.requestOptions).pipe(
      tap(_ => console.log(`posted apptokens for ${from} at ${this.apptpkensUrl}`)),
      catchError(this.handleError<any[]>(`addApptoken`))
    );
  }

  // DELETE apptokens/{id}
  deleteApptoken(id: string, from: string = "") {
    const url = `${this.apptpkensUrl}/${id}`
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted apptoken id="${id}" for ${from} at ${this.apptpkensUrl}`)),
      catchError(this.handleError('deleteApptoken', []))
    );
  }

  // GET sshkeys/
  getSSHKeys(from: string = ""): Observable<any[]> {
    return this.http.get<any[]>(this.sshUrl, this.requestOptions).pipe(
      tap(_ => console.log(`fetched sshkeys for ${from} at ${this.sshUrl}`)),
      catchError(this.handleError<any[]>(`getSSHKeys`, []))
    );
  }

  // POST sshkeys/
  addSSHKey(key, from: string = ""): Observable<any> {
    return this.http.post<any>(this.sshUrl, key, this.requestOptions).pipe(
      tap(_ => console.log(`posted sshkey for ${from} at ${this.sshUrl}`)),
      catchError(this.handleError<any[]>(`addSSHKey`))
    );
  }

  // DELETE sshkeys/{id}
  deleteSSHKey(id: string, from: string = "") {
    const url = `${this.sshUrl}/${id}`
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted sshkey id="${id}" for ${from} at ${this.sshUrl}`)),
      catchError(this.handleError('deleteSSHKey', []))
    );
  }

  // POST capsules/{id}/sshkeys
  addSSHKeyToCapsule(capsuleId: string, key, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/sshkeys`
    return this.http.post<any>(url, key, this.requestOptions).pipe(
      tap(_ => console.log(`posted sshkey for ${from} at ${url}`)),
      catchError(this.handleError('addSSHKeyToCapsule'))
    )
  }

  // DELETE capsules/{id}/sshkeys/{id}
  deleteSSHKeyToCapsule(capsuleId: string, keyId: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/sshkeys/${keyId}`
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted sshkey id="${keyId}" for ${from} at ${url}`)),
      catchError(this.handleError('deleteSSHKeyToCapsule', []))
    )
  }

  // GET users/{name}
  getUserByName(name: string, from: string = "") {
    const url = `${this.userUrl}/${name}`
    return this.http.get<any>(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched users for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getUserByName`))
    )
  }

  // GET /me
  getCurrentUser(from: string = "") {
    const url = `${this.baseUrl}/me`
    return this.http.get(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched current user for ${from} at ${url}`)),
      catchError(this.handleError<any[]>(`getCurrentUser`))
    )
  }

  // GET capsules/{id}/webapp/crons
  getWebappCrons(id: string, from: string = "") {
    const url = `${this.capsulesUrl}/${id}/webapp/crons`;
    return this.http.get(url, this.requestOptions).pipe(
      tap(_ => console.log(`fetched crons id=${id} for ${from} at ${url}`)),
      catchError(this.handleError(`getWebappCrons from=${from} id=${id}`))
    );
  }

  // POST capsules/{id}/webapp/crons
  addWebappCron(cron: any, capsuleId: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/webapp/crons`
    return this.http.post<any>(url, cron, this.requestOptions).pipe(
      tap(_ => console.log(`posted cron for ${from} at ${url}`)),
      catchError(this.handleError('addWebappCron'))
    );
  }

  putWebappCron(cron: object, capsuleId: string, cronId: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/webapp/crons/${cronId}`
    return this.http.put<any>(url, cron, this.requestOptions).pipe(
      tap(_ => console.log(`put cron for ${from} at ${url}`)),
      catchError(this.handleError('putWebappCron'))
    );
  }

  deleteWebappCron(capsuleId: string, cronId: string, from: string = "") {
    const url = `${this.capsulesUrl}/${capsuleId}/webapp/crons/${cronId}`;
    return this.http.delete(url, this.requestOptions).pipe(
      tap(_ => console.log(`deleted cron for ${from} at ${url}`)),
      catchError(this.handleError('deleteWebappCron', []))
    );
  }

  /**
   * Prints the error on console
   * @param operation Failed function's name
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any) => {

      console.error(error); // log to console instead

      console.log(`${operation} failed: ${error.message}`);

      environment.status = error.status

      // Let the app keep running by returning an empty result.
      return [null];
    };

  }
}
