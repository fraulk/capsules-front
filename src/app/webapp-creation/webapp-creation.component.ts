import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef, SimpleChanges } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-webapp-creation',
  templateUrl: './webapp-creation.component.html',
  styleUrls: ['./webapp-creation.component.scss']
})
export class WebappCreationComponent implements OnInit {
  @Input() webappList
  @Input() lock: boolean = false
  @Output() readyWebapp: EventEmitter<object> = new EventEmitter()
  // @Output() webappChanged = new EventEmitter()
  selectedRuntime: any
  selectContent: any
  finalWebapp: any
  fqdn: any
  alias: any
  error: any
  errorMsg1: any; errorMsg2: any;
  isIncorrect: boolean = false


  constructor(private toastrService: NbToastrService, public translate: TranslateService) { }

  ngOnInit(): void {
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validFQDN').subscribe(res => this.errorMsg1 = res)
    this.translate.get('ERROR.validAlias').subscribe(res => this.errorMsg2 = res)
  }

  ngOnChanges() {
    //FIXME detect change and reset input
    this.selectedRuntime = null
    if (this.selectContent != null) this.selectContent.reset()
  }

  onFamSelect(runtime, select) {
    this.selectedRuntime = runtime
    this.selectContent = select
  }

  /**
   * Control input, create object, emit an event telling the webapp object is ready to post
   * @param webappName 
   * @param fqdn 
   * @param alias 
   */
  add(webappName: string, fqdn: string, alias: string) {
    this.isIncorrect = false
    let inputCtrlFqdn = fqdn.match(/^(?!:\/\/)([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-][a-zA-Z0-9-]+\.[a-zA-Z]{2,6}?$/g)
    if (inputCtrlFqdn == null) {
      this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' });
      this.isIncorrect = true
      return
    }
    if (this.selectedRuntime == null)
      return
    let newWebapp =
    {
      "fqdns": [
        {
          "alias": false,
          "name": fqdn
        }
      ],
      "runtime_id": this.selectedRuntime.id
    }
    if (alias != undefined && alias != "") {
      let inputCtrlAlias = alias.match(/^(?!:\/\/)([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-][a-zA-Z0-9-]+\.[a-zA-Z]{2,6}?$/g)
      if (inputCtrlAlias == null && alias != "") {
        this.toastrService.show(this.errorMsg2, this.error, { status: 'danger' });
        this.isIncorrect = true
        return
      }
      newWebapp.fqdns.push({ "alias": true, "name": alias })
    }
    this.finalWebapp = newWebapp
    this.readyWebapp.emit(newWebapp)
  }

  setFQDN(fqdn) {
    this.fqdn = fqdn
  }

  setAlias(alias) {
    this.alias = alias
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validFQDN').subscribe(res => this.errorMsg1 = res)
      this.translate.get('ERROR.validAlias').subscribe(res => this.errorMsg2 = res)
    });
  }

}
