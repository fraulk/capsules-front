import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebappCreationComponent } from './webapp-creation.component';

describe('WebappCreationComponent', () => {
  let component: WebappCreationComponent;
  let fixture: ComponentFixture<WebappCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebappCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebappCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
