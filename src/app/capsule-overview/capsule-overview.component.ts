import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter, TemplateRef, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { NbToastrService, NbDialogService } from '@nebular/theme';
import { Capsule } from '../objects/capsule';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { CapsuleService } from '../capsule.service';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-capsule-overview',
  templateUrl: './capsule-overview.component.html',
  styleUrls: ['./capsule-overview.component.scss']
})
export class CapsuleOverviewComponent implements OnInit {
  @Input() capsule: Capsule;
  @Input('cdkDragFreeDragPosition') freeDragPosition: { x: number; y: number; }
  @Input() hasAddons: boolean = true
  @Output() refresh = new EventEmitter();
  users = []
  error: string
  errorMsg: string; errorMsg1: string; errorMsg2: string; errorMsg3: any;
  success: string
  successMsg: string; successMsg2: string;
  editMode: boolean = false
  overviewCardAccent: string = "primary"
  defaultPosition = { x: 0, y: 0 };
  resetDisabled: boolean = true
  newCapsOwner: string[]
  newCapsKeys: any[]
  sshKeyToShow: string;
  role: string = 'user'


  constructor(private toastrService: NbToastrService, public translate: TranslateService, private capsuleService: CapsuleService, private dialogService: NbDialogService, private titleService: Title, private cd: ChangeDetectorRef, private router: Router) { }

  addOwnerToCapsule(owner) {
    this.capsuleService.patchUserByCapsule(this.capsule.id, owner, "capsule-overview").subscribe(
      (capsule) => {
        if (capsule != null)
          this.newCapsOwner = capsule.owners
        else
          this.newCapsOwner = this.capsule.owners
      },
      (err) => console.log(err),
      () => this.capsule.owners = this.newCapsOwner
    )
  }

  deleteOwnerToCapsule(owner) {
    this.capsuleService.deleteUserByCapsule(this.capsule.id, owner, "capsule-overview").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.refresh.emit()
        this.cd.detectChanges()
      }
    )
  }

  addSSHKeyToCapsule(key) {
    this.capsuleService.addSSHKeyToCapsule(this.capsule.id, key, "capsule-overview").subscribe(
      (capsule) => this.newCapsKeys = capsule.authorized_keys,
      (err) => console.log(err),
      () => this.capsule.authorized_keys = this.newCapsKeys
    )
  }

  deleteSSHKeyToCapsule(keyId) {
    this.capsuleService.deleteSSHKeyToCapsule(this.capsule.id, keyId, "capsule-overview").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.refresh.emit()
        this.cd.detectChanges()
      }
    )
  }

  resetPosition() {
    this.freeDragPosition = { x: 0, y: 0 };
    this.resetDisabled = true
    localStorage.setItem('OverviewCardPos', JSON.stringify(this.freeDragPosition))
  }

  afterDrag($ev: CdkDragEnd) {
    this.freeDragPosition = $ev.source.getFreeDragPosition()
    if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
    localStorage.setItem('OverviewCardPos', JSON.stringify(this.freeDragPosition))
  }

  edit() {
    this.editMode = !this.editMode
    this.overviewCardAccent = "warning"
  }

  /**
   * Save the new owner
   * @param value new owner
   */
  onSave(value) {
    if (!value) {
      this.toastrService.show(this.errorMsg, this.error, { status: 'danger' });
      return
    }
    let inptControl = (value.match(/[A-Za-z0-9\-_]+/m) != null) ? value.match(/[A-Za-z0-9\-_]+/m)[0] : ""
    if (value == inptControl) {
      this.addOwnerToCapsule({ "newOwner": value })
      this.toastrService.show(this.successMsg, this.success, { status: 'success' });
    }
    else {
      this.toastrService.show(this.errorMsg1, this.error, { status: 'danger' });
      return
    }
    this.overviewCardAccent = "success"
    this.editMode = false
  }

  /**
   * Save ssh
   * @param key ssh key to add
   */
  onSaveSSH(key) {
    if (!key) {
      this.toastrService.show(this.errorMsg, this.error, { status: 'danger' });
      return
    }
    let inputControl = key.match(/ssh-rsa AAAA[0-9A-Za-z+/]+[=]{0,3} ([^@]+@[^@]+)/gm)
    if (key == inputControl) {
      this.addSSHKeyToCapsule([key])
      this.toastrService.show(this.successMsg, this.success, { status: 'success' });
    }
    else {
      this.toastrService.show(this.errorMsg3, this.error, { status: 'danger' });
      return
    }
    this.overviewCardAccent = "success"
    this.editMode = false
  }

  onCancel() {
    this.overviewCardAccent = "danger"
    this.editMode = false
  }

  onDelete() {
    this.capsuleService.deleteCapsule(this.capsule.id, "capsule-overview")
      .subscribe(
        capsule => { },
        (err) => console.log(err),
        () => {
          this.refresh.emit("delete")
          this.overviewCardAccent = "success"
          this.editMode = false
        }
      )
  }

  onDeleteUser(name: string) {
    this.deleteOwnerToCapsule(name)
    this.overviewCardAccent = "success"
    this.editMode = false
  }

  onDeleteSSH(id: string) {
    this.deleteSSHKeyToCapsule(id)
    this.overviewCardAccent = "success"
    this.editMode = false
  }

  open(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog)
  }

  /**
   * Asks for confirmation by typing capsule's name, then delete the capsule
   * @param dialog dialog html element
   * @param hasConfirmed Confirm or cancel
   * @param inputValue Input to check
   */
  close(dialog, hasConfirmed: boolean, inputValue: string = "") {
    if (!hasConfirmed) {
      dialog.close()
      return
    }
    if (inputValue == this.capsule.name) {
      this.onDelete()
      dialog.close()
      this.toastrService.show(this.successMsg2, this.success, { status: 'success' });
    }
    else
      this.toastrService.show(this.errorMsg2, this.error, { status: 'danger' });
  }

  openSSH(dialog: TemplateRef<any>, sshkey: string) {
    this.sshKeyToShow = sshkey
    this.dialogService.open(dialog)
  }

  closeSSH(dialog) {
    dialog.close()
  }

  ngOnInit(): void {
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
    this.translate.get('SUCCESS.successDelete').subscribe(res => this.successMsg2 = res)
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('ERROR.validElement').subscribe(res => this.errorMsg = res)
    this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg1 = res)
    this.translate.get('WEBAPP.webappChangeError').subscribe(res => this.errorMsg2 = res)
    this.translate.get('ERROR.validSSHKey').subscribe(res => this.errorMsg3 = res)
    this.freeDragPosition = JSON.parse(localStorage.getItem('OverviewCardPos'))
    if (this.freeDragPosition == undefined) this.freeDragPosition = { x: 0, y: 0 }
    if (this.freeDragPosition.x != 0 || this.freeDragPosition.y != 0) this.resetDisabled = false
    this.titleService.setTitle("Capsule Front - Overview")
    this.role = sessionStorage.getItem("role")
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res)
      this.translate.get('SUCCESS.successDelete').subscribe(res => this.successMsg2 = res)
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('ERROR.validElement').subscribe(res => this.errorMsg = res)
      this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg1 = res)
      this.translate.get('WEBAPP.webappChangeError').subscribe(res => this.errorMsg2 = res)
      this.translate.get('ERROR.validSSHKey').subscribe(res => this.errorMsg3 = res)
    });
  }

  ngOnChanges() {
    this.editMode = false
  }

}
