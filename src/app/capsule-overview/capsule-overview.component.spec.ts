import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapsuleOverviewComponent } from './capsule-overview.component';

describe('CapsuleOverviewComponent', () => {
  let component: CapsuleOverviewComponent;
  let fixture: ComponentFixture<CapsuleOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapsuleOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapsuleOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
