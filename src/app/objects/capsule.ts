import { Addons } from './addons'
import { Webapp } from './webapp';

export interface Capsule {
    id: string,
    name: string,
    owners: string[],
    webapp: Webapp,
    addons: Addons[],
    authorized_keys: any[]
}