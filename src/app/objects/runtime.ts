export interface Runtime {
    availableOpts: string[],
    created_at: string,
    family: string,
    id: string,
    infos: string,
    runtimeType: string,
    updated_at: string,
    version: string,
}