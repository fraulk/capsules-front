export interface Addons {
    id: number,
    name: string,
    description: string,
    uri: string,
    runtimeId: string,
    env: {
        HTTPS_PROXY: string,
        HTTP_PROXY: string,
    },
    opts: string[],
    quota: {
        volumeSize: number,
        memoryMax: number,
        cpuMax: number,
    }
}