export interface Webapp {
    id: string,
    insecure: boolean,
    fqdns: any,
    runtime_id: string,
    opts: [{
        tag: string,
        field_name: string,
        value: any
    }],
    quota: {
        volumeSize: number,
        memoryMax: number,
        cpuMax: number,
    },
    tls_redirect_https: boolean,
    env: {},
    created_at: string,
    updated_at: string,
}