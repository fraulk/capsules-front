import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CapsuleService } from '../capsule.service';
import { Title } from '@angular/platform-browser';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-runtimes',
  templateUrl: './runtimes.component.html',
  styleUrls: ['./runtimes.component.scss']
})
export class RuntimesComponent implements OnInit {
  runtimes: any[] = []
  selectedRuntime = null
  isLoading: Boolean = true
  imgPath: string
  @ViewChild('card', { static: false }) card: ElementRef;

  constructor(private capsuleService: CapsuleService, private titleService: Title, public breakpointObserver: BreakpointObserver) { }

  getRuntimes(): void {
    this.capsuleService.getRuntimes("runtimes")
      .subscribe(
        runtimes => this.runtimes = runtimes,
        (err) => console.log(err),
        () => {
          this.sortRuntimes()
          this.isLoading = false
        }
      );
  }


  ngOnInit(): void {
    this.getRuntimes()
    this.titleService.setTitle("Capsule Front - Runtimes")
  }

  /**
   * Shows the right card with runtime data, adding some animation
   * @param runtime selected runtime
   */
  showDetails(runtime) {
    if (this.card != undefined) {
      this.card.nativeElement.style.animation = "marginRight .3s"
      setTimeout(() => {
        this.selectedRuntime = runtime
        this.setImg(runtime.fam)
        this.card.nativeElement.style.animation = "marginLeft .3s"
      }, 300);
    } else {
      this.selectedRuntime = runtime
      this.setImg(runtime.fam)
    }
  }

  /**
   * Sorts runtimes alphabetically by runtimes family
   */
  sortRuntimes() {
    this.runtimes = this.runtimes.sort((a, b): number => {
      if (a.fam.toLowerCase() < b.fam.toLowerCase()) return -1
      if (a.fam.toLowerCase() > b.fam.toLowerCase()) return 1
      return 0
    })
  }

  setImg(name: string) {
    switch (name.toLowerCase()) {
      case "apache php":
        this.imgPath = "../assets/square-php-apache.svg";
        break;
      case "nodejs":
        this.imgPath = "../assets/square-nodejs.svg";
        break;
      case "ruby":
        this.imgPath = "../assets/square-ruby.svg";
        break;
      case "mysql":
        this.imgPath = "../assets/square-mysql.svg"
        break;
      case "postgresql":
        this.imgPath = "../assets/square-psql.svg"
        break;
      case "redis":
        this.imgPath = "../assets/square-redis.svg"
        break;
      case "memcached":
        this.imgPath = "../assets/square-mem.svg"
        break;
      default:
        this.imgPath = "";
        break;
    }
  }

}
