import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuntimesComponent } from './runtimes.component';

describe('RuntimesComponent', () => {
  let component: RuntimesComponent;
  let fixture: ComponentFixture<RuntimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuntimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuntimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
