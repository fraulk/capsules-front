# TODO global

access token url
[http://capsule-api.lss1.backbone.education/auth/realms/ac-ver-test/protocol/openid-connect/token]

client-id : api-get-users
secret : b703de9d-00a8-4a80-9c65-14c2f82638e3
scope : openid

faire les retour a la ligne dans addons (prio 1)  

~~addon info reprendre les variables temp pour remplir les new modif de addon au lieu ade mettre en input value~~  
~~addon info afficher la liste des options dans le bloc options disponibles (mettre entre paretnthese si cest default ou current)~~  
~~fqdn pouvoir en supprimer et print si c'est alias ou non (possibilité de choisir si c'est un fqdn ou alias)~~  
~~mettre tabs pour crons si possible~~  
~~menu gauche continuer a bosser dessus~~  

~~Option de la webapp a rajouter : FQDN, var d'ENV, tls certif, tls key, tls toggle (prio 4)~~  
~~A la suppression d'une capsule, elle reste visible... (prio 4)~~  
~~Il faut mettre un alias pour valider la webapp, pourtant il n'est pas obligatoire. (prio 4)~~  
~~La version d'une webapp (valable aussi pour addon) correspond au champ name. (prio 4)~~  
~~La description pourrait être affiché dans une boite sur le côté ou au survol d'un petit "i" à droite (sur addon aussi). (prio 3)~~  
~~pb avec l'ajout d'addon ? (Ils ne s'affichent pas tout le temps alors qu'ils sont bien en base et avec Postman) (prio 4)~~ : impossible de reproduire le bug, peut etre deja fix avec les commits precedents  
~~Le tableau ne s'affiche pas tout seul après saisie du premier token, il faut refresh. (même problème pour les sshkeys) (prio 3)~~  
~~la validation d'un proprio (avec mauvais id) entraîne deux messages : ajouté puis pas bon, il faudrait mettre le valide quand c'est le cas uniquement. (prio 3)~~  
~~Après l'étape 3, il serait vraiment bien d'arriver sur la nouvelle capsule et pas sur la 1ère de la liste. (prio 3)~~  
~~empecher de repasser sur overview apres put avec opts sur addons (prio 3)~~  
~~il y a un pb de cohérence entre les blocs d'options, les valeurs saisies et les valeurs par défaut. (prio 3)~~  
~~webapp/addon mettre les valeurs de base de webapp si elle existe dans opts (prio 3)(ajouter au put aussi)~~  
~~Il manque la gestion des crons dans les capsules, on avait laissé de côté. (prio 3)~~  
~~Le message "LAPI ne répond pas" lorsque l'on n'a pas de capsule. Il faudrait plutôt un message "Vous n'avez pas de capsule, cliquez sur le bouton ci dessous pour en créer une" (ou demandez à un admin de vous en créer une). (prio 2)~~  
~~si le nom de capsule ne respecte pas "alphanumérique minuscule et '-'", il faudrait pouvoir le savoir dès la première étape, pas à la fin. (prio 2)~~  
~~Après édition, on revient sur l'overview, c'est moche. (ce n'était pas le cas pour les addons dans la version d'hier) (prio 2)~~  
~~afficher le nom et description sur current webapp (prio 2)~~  
~~ajouter les infos (desc, name) de runtime dans addon info (dans le header à droite) et desc au survol de l'image (prio 2)~~  
~~ne pas afficher webapp/addon-creation si list pas select (prio 2)~~  
~~Le bouton "Runtime" serait mieux en bas avec App tokens et sshkeys. (prio 1)~~  
~~Le message d'erreur pourrait préciser que les caractères sont alphanumériques. (prio 1)~~  
~~la visualisation est moche. Le texte fait des retour à la ligne uniquement sur certains caractères, du coup sur un petit écran ça déborde. Soit il est possible de "wrapper" proprement, soit on vire la prévisualisation et on ne garde que les boutons copier/supprimer. (prio 1)~~  
~~le placeholder pour le nom est à modifier : "John Doe" n'est pas valide et pas représentatif. "lyc-foch-versailles" serait mieux. (prio 1)~~  
~~idem pour le placeholder proprio : "pnom" serait pas mal. (prio 1)~~  
  
~~4 : faire l'authentification~~  
~~enlever css pour ssh sur overview~~  
~~4 : faire un docker complet avec ng~~  
~~add-capsule fixer le texte "Appuyer entrer pur ajouter element"~~  
~~cacher le menu lors add-capsule~~  
~~3 : faire un get user avant d'ajouter un user dans add-capsule et suppr sinon~~  
~~separer apptoken et sshkey dans menu~~  
~~noter adresse api dans environment.ts~~  
~~add-capsule pouvoir deselect webapp~~  
~~supprimer un proprio de capsule~~  
~~addon info mettre en value au lieu de placeholder~~  
~~remettre sur overview lors du changement de capsule~~  
~~uri auto a mettre addon accordion~~  
~~webapp supprimer en plus de changer~~  
~~liste des options affiche sans modif (peut etre vielle db qui cause le bug)~~  
~~3 : replacer le perso a la bonne capsule apres refresh~~  
~~addons ne se mettent pas en true après ajout~~
~~confirmation rapide lors de la suppression d'addons~~  
~~runtimes trier alphabetiquement~~  
~~runtimes trier par tag puis versions comme available_opts (affichage du nom des runtimes)~~  
