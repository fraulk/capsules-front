import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, TemplateRef, Output, EventEmitter, ViewChild, SimpleChanges } from "@angular/core";
import { Webapp } from "../objects/webapp";
import { CapsuleService } from '../capsule.service';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { WebappCreationComponent } from '../webapp-creation/webapp-creation.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: "app-webapp",
  templateUrl: "./webapp.component.html",
  styleUrls: ["./webapp.component.scss"]
})
export class WebappComponent implements OnInit {
  @Input() capsuleId: string;
  @Input() capName: string = ""
  @Input() hasWebapp: boolean = true
  @Output() needRefresh = new EventEmitter()
  @ViewChild(WebappCreationComponent) webappCreationComp: WebappCreationComponent
  webapp: Webapp = null
  runtimes: any[]
  crons: any[]
  webappName = ""
  newWebappName: string
  radioBtnChecked
  isLoading: boolean = true
  gotData: boolean = false
  selectedRuntime
  confirmedWebapp
  aliasField: boolean = false
  success: string
  successMsg: string
  error: string
  errorMsg: string
  availableOpts: any;
  fileReader: FileReader = new FileReader()
  options: any[] = []
  fileToShow: string;
  edited: boolean = false
  cronsEdited: boolean = false
  alias: object[] = []

  constructor(private capsuleService: CapsuleService, private cd: ChangeDetectorRef, private dialogService: NbDialogService, private toastrService: NbToastrService, public translate: TranslateService, private router: Router, private titleService: Title) { }

  getWebapp(capsuleId): void {
    this.capsuleService.getWebapp(capsuleId, "webapp").subscribe(
      webapp => this.webapp = webapp,
      (err) => console.log(err),
      () => {
        if (this.webapp != null) {
          this.isLoading = false
          this.options = this.webapp.opts
          this.getRuntimesFam(this.webapp.runtime_id)
          this.getCrons()
          this.cd.detectChanges()
        } else
          this.hasWebapp = false
      }
    );
  }

  deleteWebapp(newWebapp: object = null): void {
    this.capsuleService.deleteWebapp(this.capsuleId, "webapp").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        if (newWebapp != null)
          this.addWebapp(newWebapp)
        else
          this.needRefresh.emit("hasWebappFalse")
      }
    )
  }

  addWebapp(newWebapp): void {
    this.capsuleService.addWebapp(newWebapp, this.capsuleId, "webapp").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.router.navigate(['/'])
        this.needRefresh.emit("hasWebappTrue")
      }
    )
  }

  putWebapp(newWebapp): void {
    this.capsuleService.putWebapp(newWebapp, this.capsuleId, "webapp").subscribe(
      () => { },
      (err) => console.log(err),
      () => {
        this.router.navigate(['/'])
        this.needRefresh.emit()
      }
    )
  }

  getRuntimesFam(runId: string) {
    this.capsuleService.getRuntime(runId, "webapp").subscribe(
      runtime => this.availableOpts = (runtime.available_opts.length != 0) ? runtime.available_opts : null,
      (err) => console.log(err),
      () => {
        if (this.availableOpts != null)
          this.availableOpts = this.organizeOpts()
      }
    )
  }

  getCrons() {
    this.capsuleService.getWebappCrons(this.capsuleId, "webapp").subscribe(
      (crons) => this.crons = crons,
      (err) => console.log(err),
      () => { }
    )
  }

  addCrons(newCron) {
    this.capsuleService.addWebappCron(newCron, this.capsuleId, "webapp").subscribe(
      (crons) => {
        if (this.crons == null) this.crons = [crons]
        else this.crons.push(crons)
      },
      (err) => console.log(err),
      () => { }
    )
  }

  putCrons(cronId, newCron) {
    this.capsuleService.putWebappCron(newCron, this.capsuleId, cronId, "webapp").subscribe(
      () => { },
      (err) => console.log(err),
      () => this.getCrons()
    )
  }

  deleteCron(cronId) {
    this.capsuleService.deleteWebappCron(this.capsuleId, cronId, "webapp").subscribe(
      () => { },
      (err) => console.log(err),
      () => this.getCrons()
    )
  }

  ngOnInit(): void {
    if (this.capsuleId != null && this.hasWebapp)
      this.getWebapp(this.capsuleId)
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
    this.translate.get('WEBAPP.webappChangeSuccess').subscribe(res => this.successMsg = res)
    this.translate.get('ERROR.error').subscribe(res => this.error = res)
    this.translate.get('WEBAPP.webappChangeError').subscribe(res => this.errorMsg = res)
    this.titleService.setTitle("Capsule Front - Webapp")
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      let chng = changes[propName]
      if (!chng.firstChange) {
        if (this.hasWebapp == true) this.getWebapp(this.capsuleId)
      }
    }
  }

  onEnter(value: string) {
    if (value) {
      this.webapp.fqdns.push(value);
      value = "";
    }
  }

  open(dialog: TemplateRef<any>, dialog2: TemplateRef<any>, radioBtnValue: any, radioBtnChecked) {
    this.newWebappName = radioBtnValue
    this.radioBtnChecked = radioBtnChecked
    // this.dialogService.open(dialog2).onClose.subscribe(_ => this.radioBtnChecked.checked = false)
    this.dialogService.open(dialog).onClose.subscribe(_ => this.radioBtnChecked.checked = false)
  }

  /**
   * Dialog for setting the new webapp, after confirming the change by typing capsule's name
   * @param dialog nb-dialog html element
   * @param hasConfirmed confirmed or canceled
   * @param inputValue input's value
   * @param webappName webapp's name
   * @param fqdn fqdn value
   * @param alias alias value
   */
  close(dialog, hasConfirmed: boolean, inputValue: string = "", webappName = "", fqdn = null, alias = "") {
    if (!hasConfirmed) {
      dialog.close()
      return
    }
    if (inputValue == this.capName) {
      this.webappCreationComp.add(webappName, this.webappCreationComp.fqdn, this.webappCreationComp.alias)
      this.confirmedWebapp = this.webappCreationComp.finalWebapp
      if (this.webappCreationComp.isIncorrect) return
      this.deleteWebapp(this.confirmedWebapp)
      dialog.close()
      this.toastrService.show(this.successMsg, this.success, { status: 'success' });
    }
    else
      this.toastrService.show(this.errorMsg, this.error, { status: 'danger' });
  }

  openDeleteDialog(dialog: TemplateRef<any>) {
    this.dialogService.open(dialog)
  }

  closeDeleteDialog(dialog, hasConfirmed: boolean, inputValue: string = "") {
    if (!hasConfirmed) {
      dialog.close()
      return
    }
    if (inputValue == this.capName) {
      this.deleteWebapp()
      dialog.close()
      this.toastrService.show(this.successMsg, this.success, { status: 'success' })
    }
  }

  add(newWebapp: object) {
    this.addWebapp(newWebapp)
    this.toastrService.show(this.successMsg, this.success, { status: 'success' });
  }

  confirm(newWebapp: object) {
    this.confirmedWebapp = newWebapp
  }

  showParameters() {
    this.gotData = true
  }

  onFamSelect(runtime, select) {
    this.selectedRuntime = runtime
  }

  /**
   * Used for converting file in base 64
   * @param file file to convert in base 64
  */
  toBase64 = file => new Promise((resolve, reject) => {
    this.fileReader.readAsDataURL(file);
    this.fileReader.onload = () => resolve(this.fileReader.result);
    this.fileReader.onerror = error => reject(error);
  });

  /**
   * Saves the data for each options.
   * If option array is empty, push a new element; else it search for an existing option and put the value on or delete it if value's empty; else create an element if not found
   * @param field_name the unique field name
   * @param tag the tag of the option
   * @param value the input value
   * @param file true if it's a file
  */
  // TODO input control for options
  async saveOptions(field_name: string, tag: string, value: any, file: boolean = false) {
    if (file) { value = await this.toBase64(value[0]) }
    if (value == true || value == false) { value = `${value}` }
    let found = -1
    if (this.options.length == 0)
      this.options.push({ "field_name": field_name, "tag": tag, "value": value })
    else {
      this.options.map((item, i) => {
        if (item.field_name == field_name) {
          found = i
          return
        }
      })
      if (found > -1) {
        if (value != "")
          this.options[found].value = value
        else
          this.options.splice(found, 1)
      }
      else
        this.options.push({ "field_name": field_name, "tag": tag, "value": value })
    }
  }

  saveWebappOptions() {
    let newWebapp =
    {
      "fqdns": [this.webapp.fqdns[0]],
      "runtime_id": this.webapp.runtime_id
    }
    if (this.options.length != 0)
      newWebapp["opts"] = this.options
    this.putWebapp(newWebapp)
  }

  /**
  * Organize array sorted by tags
  * @returns Returns a list of tags
  */
  organizeOpts(): any[] {
    let tags = []
    let tagsName
    let newTags: object = {}
    let keys = []

    this.availableOpts.map(item => tags.push(item.tag))
    tagsName = [...new Set(tags)] // removes duplicates
    tagsName.map(item => newTags[item] = []) // create an array of object with tag name as key
    tags = []
    tags.push(newTags)
    keys = Object.keys(tags[0]) // create an array of keys as string
    this.availableOpts.map(item => {
      keys.map(key => {
        if (item.tag == key)  // if the tag match the key, push all the stuff into our organized array
          tags[0][key].push(item)
        this.webapp.opts.map(webappOpts => {
          if (item.tag == webappOpts.tag) { // if it matches the same tags between selected webapps tags and available opts tags
            tags[0][key].map((itemOpts, i) => { // map the previously created array
              if (itemOpts.field_name == webappOpts.field_name) // if available options field names matches selected webapps field name
                itemOpts['current_value'] = webappOpts.value // add the current value field to remember previous data
            })
          }
        })
      })
    })
    tags.push(tagsName)
    console.log(tags)
    return tags
  }

  /**
   * Shows an existent file in the webapp's options
   * @param dialog nb-dialog html element
   * @param file file data from options
   */
  openFileDialog(dialog: TemplateRef<any>, file: string) {
    file = file.substr(file.indexOf(',') + 1)
    this.fileToShow = atob(file)
    this.dialogService.open(dialog)
  }

  closeFileDialog(dialog) {
    dialog.close()
  }

  isEmpty(obj) {
    return Object.keys(obj).length === 0;
  }

  addEnvVar(key, value) {
    if (key == "") return
    this.edited = true
    this.webapp.env[key] = value
  }

  deleteEnvVar(key) {
    this.edited = true
    delete this.webapp.env[key]
  }

  editKey(key, newKey, value) {
    this.edited = true
    this.deleteEnvVar(key)
    this.addEnvVar(newKey, value)
  }

  editValue(key, value) {
    this.edited = true
    this.webapp.env[key] = value
  }

  isEdited() {
    this.edited = true
  }

  removeFqdn(values, i) {
    values.splice(i, 1)
    this.edited = true
  }

  editAlias(values, i, isCheck) {
    values[i]['alias'] = isCheck
    console.log(values[i].alias)
    this.edited = true
  }

  addOnFqdn(value, values, i) {
    this.webapp.fqdns.push({ "alias": true, "name": value })
    values.splice(i, 1)
  }

  /**
   * Saves the new parameters for the webapp
   * @param httpRedirect 
   * @param tlsCert 
   * @param tlsKey 
   * @param fqdns 
   */
  saveParameters(httpRedirect: boolean, tlsCert: string, tlsKey: string, fqdns) {
    let newWebapp =
    {
      "tls_redirect_https": httpRedirect,
      "fqdns": this.webapp.fqdns,
      "runtime_id": this.webapp.runtime_id
    }
    if (tlsCert != "" && tlsKey != "") {
      newWebapp['tls_crt'] = btoa(tlsCert)
      newWebapp['tls_key'] = btoa(tlsKey)
    }
    fqdns.map((item, i) => {
      let fqdnsObj = { "alias": this.alias[i]['alias'], "name": item }
      newWebapp.fqdns.push(fqdnsObj)
    })
    if (this.webapp.opts.length > 0)
      newWebapp['opts'] = this.webapp.opts
    if (!this.isEmpty(this.webapp.env))
      newWebapp['env'] = this.webapp.env
    this.putWebapp(newWebapp)
    console.log(newWebapp)
  }

  /**
   * Saves the existent crons modifications to the webapp
   * @param command 
   * @param hour 
   * @param minute 
   * @param month 
   * @param monthDay 
   * @param weekDay 
   * @param cronId 
   */
  saveCron(command: string, hour: string, minute: string, month: string, monthDay: string, weekDay: string, cronId: string) {
    if (command == "" || hour == "" || minute == "" || month == "" || monthDay == "" || weekDay == "") return // TODO add toastr with error msg
    let newCron = {
      "command": command,
      "hour": hour,
      "minute": minute,
      "month": month,
      "month_day": monthDay,
      "week_day": weekDay
    }
    this.putCrons(cronId, newCron)
  }

  /**
   * Saves the new crons to the webapp (should be merge with the saveCron func, put if cronId param given else post)
   * @param command 
   * @param hour 
   * @param minute 
   * @param month 
   * @param monthDay 
   * @param weekDay 
   */
  saveNewCron(command: string, hour: string, minute: string, month: string, monthDay: string, weekDay: string) {
    // console.log(command, hour, minute, month, monthDay, weekDay)
    if (command == "" || hour == "" || minute == "" || month == "" || monthDay == "" || weekDay == "") return // TODO add toastr with error msg
    let newCron = {
      "command": command,
      "hour": hour,
      "minute": minute,
      "month": month,
      "month_day": monthDay,
      "week_day": weekDay
    }
    this.addCrons(newCron)
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res)
      this.translate.get('WEBAPP.webappChangeSuccess').subscribe(res => this.successMsg = res)
      this.translate.get('ERROR.error').subscribe(res => this.error = res)
      this.translate.get('WEBAPP.webappChangeError').subscribe(res => this.errorMsg = res)
    });
  }
}
