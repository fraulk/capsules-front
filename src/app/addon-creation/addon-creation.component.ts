import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { NbToastrService } from '@nebular/theme';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-addon-creation',
  templateUrl: './addon-creation.component.html',
  styleUrls: ['./addon-creation.component.scss']
})
export class AddonCreationComponent implements OnInit {
  @Input() addonsList: any[] = []
  @Input() lock: boolean = false
  @Output() readyAddon: EventEmitter<any> = new EventEmitter<string>()
  error: string = ""
  errorMsg2: string = ""; errorMsg3: string = ""; errorMsg4: string = ""
  success: string = ""; successMsg: string = ""
  selectedRuntime: any;
  selectContent: any = null
  name: string
  desc: string
  finalAddon: any
  isIncorrect: boolean = false

  constructor(private toastrService: NbToastrService, public translate: TranslateService) { }

  ngOnInit(): void {
    this.translate.get('ERROR.error').subscribe(res => this.error = res);
    this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
    this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg3 = res);
    this.translate.get('ERROR.missingRuntime').subscribe(res => this.errorMsg4 = res);
    this.translate.get('SUCCESS.success').subscribe(res => this.success = res);
    this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res);
  }

  ngOnChanges() {
    //FIXME detect change and reset input
    if (this.selectContent != null) this.selectContent.reset()
    this.selectedRuntime = null
  }

  onFamSelect(runtime, select) {
    this.selectedRuntime = runtime
    this.selectContent = select
  }

  /**
   * Create an addon object, after checking validity (without posting)
   * @param name Addons name
   * @param desc Addons description
   * @param addonName Addons runtimes family name (mysql, redis, ...) (unused)
   */
  add(name: any, desc: any, addonName: string) {
    this.isIncorrect = false
    if (this.selectedRuntime == null) {
      this.toastrService.show(this.errorMsg4, this.error, { status: 'danger' })
      return
    }
    if (name == "") {
      this.toastrService.show(this.errorMsg3, this.error, { status: 'danger' })
      this.isIncorrect = true
      return
    }
    let inptControl = (name.match(/[a-z0-9\-_]+/m) != null) ? name.match(/[a-z0-9\-_]+/m)[0] : ""
    if (name != inptControl) {
      this.toastrService.show(this.errorMsg2, this.error, { status: 'danger' });
      this.isIncorrect = true
      return
    }
    let newAddon =
    {
      "description": desc,
      "name": name,
      "runtime_id": this.selectedRuntime.id
    }
    this.finalAddon = newAddon
    this.readyAddon.emit(newAddon)
    this.toastrService.show(this.successMsg, this.success, { status: 'success' });
  }

  setName(name) {
    this.name = name
  }

  setDesc(desc) {
    this.desc = desc
  }

  ngAfterContentInit() {
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.translate.get('ERROR.error').subscribe(res => this.error = res);
      this.translate.get('ERROR.specialChar').subscribe(res => this.errorMsg2 = res);
      this.translate.get('ERROR.missingName').subscribe(res => this.errorMsg3 = res);
      this.translate.get('ERROR.missingRuntime').subscribe(res => this.errorMsg4 = res);
      this.translate.get('SUCCESS.success').subscribe(res => this.success = res);
      this.translate.get('SUCCESS.successAdd').subscribe(res => this.successMsg = res);
    });
  }

}
