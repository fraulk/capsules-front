import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddonCreationComponent } from './addon-creation.component';

describe('AddonCreationComponent', () => {
  let component: AddonCreationComponent;
  let fixture: ComponentFixture<AddonCreationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddonCreationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddonCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
