# CapsuleFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.3.

## Development server

Run `ng serve` (or `npm run ng serve`) for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Update Angular

You need [Angular CLI](https://cli.angular.io/) to use `ng` commands

Run those commands inside the project to update angular to the latest version

```bash
ng update @angular/cli
ng update @angular/core
ng update @angular/material
```

Also check this website for more help [Angular Update Guide](https://update.angular.io/)

## Update Node

- [Node Version Manager](https://github.com/nvm-sh/nvm)  
- [NVM for windows](https://github.com/coreybutler/nvm-windows)  

or alternatively (recommended) :

`npm install -g npm stable`

`npm install -g node`
