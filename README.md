# Capsule Front

## Summary

- [Capsule Front](#capsule-front)
  - [Summary](#summary)
  - [Deploy](#deploy)
    - [With angular-cli](#with-angular-cli)
      - [Generic nginx.conf](#generic-nginxconf)
      - [Dockerfile](#dockerfile)
    - [Without angular-cli](#without-angular-cli)
  - [Adding new picture for webapps/addons](#adding-new-picture-for-webappsaddons)
  - [Adding new themes](#adding-new-themes)
  - [Changing font](#changing-font)
  - [Change translation's text](#change-translations-text)
  - [Tips](#tips)
  - [API calls](#api-calls)

## Deploy

### With angular-cli

You need to build the project first with ```ng build --prod```, it will create a ```/dist``` directory

Write your nginx.conf with your favourite settings, but the ```location``` is important and need to be always present  
The conf file down below works

#### Generic nginx.conf

```conf
events {
        worker_connections  4096;
}

http{

    include /etc/nginx/mime.types;

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        root /usr/share/nginx/html;
        # Add index.php to the list if you are using PHP
        index index.html index.htm index.nginx-debian.html;
        server_name _;

        location / {
                # First attempt to serve request as file, then
                # as directory, then fall back to displaying a 404.
                try_files $uri $uri/ /index.html;
        }

        location ~* ^.+\.css$ {
            default_type text/css;
        }

        location ~* ^.+\.js$ {
            default_type text/javascript;
        }
    }
}
```

#### Dockerfile

```Dockerfile
FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY . /usr/share/nginx/html
```

Next, you can upload the ```/dist``` on a server

Local machine :

1. ``scp -r .\dist\capsule-front\ {user}@capsule-api.lss1.backbone.education:/home/fulker/``

Server :

1. ``sudo docker stop capsule-front-container``
2. ``sudo docker rm capsule-front-container``
3. ``sudo docker build -t capsule-front-img .``
4. ``sudo docker run --name capsule-front-container -d -p 4200:80 capsule-front-img``

### Without angular-cli

1. Clone the project
2. Go into the project directory
3. Write a Dockerfile

```Dockerfile
FROM node:14

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

RUN npm install -g @angular/cli

COPY . .

RUN npm install

RUN ng build --prod
```

4. Build the image ```sudo docker build -t node .```
5. Go outside of the project directory ``cd ..``
6. Create the container (not run it) ```sudo docker create --name angular-cli node```
7. Copy the ``/dist`` repertory to your local machine ```sudo docker cp angular-cli:/usr/src/app/dist .```

## Adding new picture for webapps/addons

There is images used in all these files for addons and/or webapp, just add a case for the switch statement and the path in the usually named `setImg` function  

- accordion-item  
- addon-info  
- addons-list  
- add-capsule  
- current-webapp  
- runtimes  
- webapp-list  

## Adding new themes

Choose the colors you want [here](https://colors.eva.design/)  
You can copy already made theme and change the colors or export from the website  
Give it a name, and the theme it is based on and put it in [themes.scss](src/themes.scss)

```scss
$nb-themes: nb-register-theme(("your theme here"), "aquamarine", dark);
```

Finally, add the theme name to the theme list variable named themeList located in [app.component.ts](src/app/app.component.ts)

## Changing font

Find an url for the font

```scss
@import url('https://fonts.googleapis.com/css?family=Roboto:300&display=swap');
```

Then add it to the theme you want to change the font, also in [themes.scss](src/themes.scss)

```scss
font-family-primary: unquote('Roboto, sans-serif'),
font-family-secondary: font-family-primary,
```

## Change translation's text

All the text is stored in those files, [english](src/assets/i18n/en.json) and [french](src/assets/i18n/fr.json).
You can add text in the html template this way `{{ 'OVERVIEW.looseData' | translate }}`  
You reference the position of the text in the json file and you add `translate` to the variable.  
You can also use an extension to do it faster ('Generate translation' in extension tab)

## Tips

There's some `TODO` and `FIXME` in the code, using an extension to find where they are could be useful, i'm using "Todo Tree"

## API calls

All the api calls are located in [capsule.service.ts](src/app/capsule.service.ts), and the base url is in [environment.ts](src/environments/environment.ts) ([environment.prod.ts](src/environments/environment.prod.ts) if prod)
